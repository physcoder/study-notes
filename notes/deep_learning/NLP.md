## Linguistic Structure
### Two Views
#### Constituency (phrase structure/ context-free grammar)
organizes words into nested contituents

- words -> phrases -> bigger phrases -> sentences
- basic units (noun, prep., det., adj., ...) can transform into longer sequences; each becomes a transformation rule, e.g. `noun -> prep. noun`
- context-free grammer rules (like decision trees) can be constructed 

#### Dependency Structure
which words depend on (modify or are arguments of) which other words

- logical/contextual dependencies between words are captured, forming a HMM-like or tree-like dependency paths.

- dependency parsing
    - tradtional: calculate sparse indicator features and use a classifier 
    - modern: neural network (fast & accurate) + the (stack + buffer) arc-standard transition-based parser structure


## Feature Extraction
### One-Hot Encoding
presence vs. absence of words

### Bag of Words
- normalize word formats 
    - lowercase
    - acronym
    - punctuation
    - stop words
    - stemming & lemmitization
- calculate word counts/ n-gram counts (term frequencies)
- construct document-term matrix
- can apply dot product or cosine similarity between two document vectors
- any model using this approach (LDA, LSA, etc) will rely on a quality preprocessing on the individual words/n-grams.

### TF-IDF
- term-frequency * inverse-document-frequency
    - tf: $\frac{count(t,D)}{|D|}$
    - idf: $log(\frac{|D|}{|{d \in D: t \in d}|})$
- variants to smooth the calculation

### Word Embedding
Static word embedding are not flexible in different contexts.

- autoencoder
    - encoder-decoder structure (in autoencoder the input and output are identical)
    - a most simple and basic way of building embedding/representations.

- word2vec
    - robust, global, distributed, reusable representation
    - size independent of vocabulary  
    - deep learning ready
    - predict missing word/missing context
    - skip-gram (better) vs. CBOW
    - ![word2vec](nlp_images/word2vec.png)
- GloVe
    - Very nice paper, read it
    - need more visual and why it's the best of both world
    - essentially factorizing the co-occurence matrix into two matrices
    - a context word vector and target word vector for each word
    - ![glove](nlp_images/glove.png) 
- t-SNE
    - t-distributed stochastic neighbor embedding
    - maintains the substructures (distances) after transformation
    - popular for visualization  
- BERT/ELMo:      
    - use intermediate outputs in hidden layers (encoders/lstms), concatenate/weighted sum to form dynamic (context-aware) word embeddings.
- Sentence/document Embedding    
    - Approach 1: Deep Averaging Networks (DAN)
        - average layer + hidden layer + softmax
        - average all word embeddings into one vector (with dropout-like mechanism)
        - may not perform well when word ordering or double negation is crucial
    - Approach 2: use `[CLS]` in BERT


## Topic Modeling
Documents consist of words, but one can assume that there is a middle layer, topics, that connects the two. Therefore, the problem is to factorize a document-term matrix into document-topic and topic-word matrices. Since the model works at the word level, generating quality and consistent bag of words features is really important.

- LSA
    - Uses SVD to decompose input document-word matrix, usually worse than LDA
- LDA
    - Has a guided-LDA version 
    - Assume each document has a mixture of topics ($p(z|d)$), and each topics is a mixture of words ($p(w|z)$). The topics $z$ are the latent variables, they help to reduce the parameter space.
    - A dirichlet distribution with small parameters (<1) would best describe typical topic distributions among documents (peak at corners; flat in between), as well as word distributions among topics.
    - Learn two dirichlet distributions to allocate
    - Algorithm:
    - ![lda algo](nlp_images/lda1.png)
    - ![lda algo](nlp_images/lda2.png)
- DMM
    - assumes one topic per document, for short texts

## Part of Speech (PoS) Tagging
tag each word in the text as verb, noun, prep., adj., ...
### Hidden Markov Model
![hmm](nlp_images/hmm.png)

- **Hidden States**: pos tags (verb, noun, ...), plus `<start>` and `<end>`
- **Emission Probabilities**: probability of each tag generating a word $P(word|tag)$
- **Transition Probabilities**: from one hidden state to the other $P(tag2|tag1)$
- **Viterbi Algorithm**: DP approach for finding the best path with highest probabilities
    - forward pass: identify paths with highest probability of reaching each hidden state, and remove the other paths
    - backward pass: start from `<end>`, work backwards and find the highest probability path till `<start>`
- `pomegranate` seems to be nice HMM package
- *Tagging*:
    - define hidden states, calculate emission and transition probabilities
    - likelihood of generating a sentence is the probability product from `<start>` to `<end>`
    - use **Viterbi** to find the highest likelihood path as the final tagging. 

## Named Entity Recognition
typically a supervised classification problem. Labels are word level entity categories from a predefined list. For each category (e.g. ORG), there are two associated labels, start & inside of a label (e.g. B-ORG, I-ORG); and an others type (O).  
Approaches:

- HMMs
- neural networks
	- CRF
	   - BI-LSTM + CRF layer + dense layer + softmax 
	   - CRF can learn constraints between labels (e.g. B- comes before I-)
	   - [Blog Link](https://createmomo.github.io/2017/09/12/CRF_Layer_on_the_Top_of_BiLSTM_1/)
   - CTC loss


## Text Sequence Processing 
**language modeling**: predicting next word in a text sequence, without need for labeling. In BERT, the task is altered to predicting masked word.

### RNN
serves as a foundational architecture, but suffered from vanishing gradients. When the model looks at the whole text sequence, preprocessing (stemming, lemmatization, etc.) are not required any more.

#### LSTM
- much more popular and usable than RNN
- 3 gates: input gate, forget gate, output gate
- in modern libraries (Keras, Tensorflow, etc.), no need to prepare the sequence input by shifting the positions any more. 
- notable model: ELMo (two-directional LSTMs + concatenation + weighted summation) for contextualized word embedding
![lstm cell](nlp_images/lstm.png)

#### GRU
- simplified version of LSTM; only 2 gates; far fewer parameters
- performs better on some tasks and smaller datasets, but still underperforms LSTM in general

#### Bi-LSTM
- process contexts in both directions

### Sequence to Sequence
It is a many-to-many mapping from input to output. A lot of applications, such as MT, QA, image captioning, text summarization, ChatBot, and etc.

- Usually follows an encoder-decoder architecture.
- Encoder learns the *context* from the input and store that in the *hidden state*
- Decoder takes the final hidden state from the Encoder, later prediction outputs depends on the previous prediction output as well 
- Different types of encoder-decoder architectures:
    - 1. the encoder output is repeated as input to each decoder cell
    - 2. prior prediction is used as input for next prediction
    - 3. prior target is used as input to next state
- ![seq2seq general structure](nlp_images/seq2seq1.png)
- ![seq2seq general structure](nlp_images/seq2seq2.png)

#### Attention + RNN
- RNN here refers to the general setup (RNN, LSTM, GRU, etc.)
- Still follows encoder-decoder structure, where attention calculation happens in the decoder, but all hidden states are passed from encoder to decoder for such calculations
- Attension means calculating weights ($score_{ij}$) at each input (source) position $j$ for the target output position $i$

##### 1. Additive Attention
[Neural Machine Translation by Jointly Learning to Align and Translate](https://arxiv.org/abs/1409.0473)

##### 2. Multiplicative Attention
[Effective Approaches to Attention-based Neural Machine Translation](https://arxiv.org/abs/1508.04025)

##### 3. Concat Attention
very similar to additive, which concatenates the decoder hidden state with each encoder hidden state and then use a dense layer and a another weight matrix to calculate the attension score.
 
---
## Self-attention + Transformer
- [Visualize Tansformers and related models](http://jalammar.github.io/illustrated-transformer/)
- [Attention I & II](https://zhuanlan.zhihu.com/p/47063917)
- [Code snippets for explaining Transformer](http://nlp.seas.harvard.edu/2018/04/03/attention.html)
- [Overview of pretraining in NLP](https://zhuanlan.zhihu.com/p/49271699) (recommended)  

Issues with RNN  

- lack of parallelization
- unable to learn long term dependence
- linear increase in operations w.r.t length of distances to remember

#### Attention (in transformer)
- solves challenge 2
- applicable to all sequence-to-sequence tasks
- basically a weighted average, but without ordering taken into account
- ideas:
    - put weights on the contextual words, rather than a constant context
    - weights are different for each input word (calculated from input/output states)
    - for each word, construct $(q, k, v)$ from the embedding input; query (q) the memory (k,v) and find the closet key (k), then weight the values (v) accordingly, finally aggregate (e.g. multiply by score and sum up) to provide context vector for current word/input
    - where variants can come in: where memory/context come from (self, encoder, etc.); how to calculate scores (dot prod, weighted dot prod, etc.); how to aggregate (weighted sum, max pooling, etc.) 
- steps:
    1. learn 3 weight matrices to calculate 3 vectors for each word (query `q`, key `k`, value `v`)
    2. `raw_score = q*k / sqrt(d)` (`d` is the input dimension; fix `q` for current word and `k` for all other words in the input sentence)
    3. `score = softmax(raw_score)` 
    4. `weighted_value = score * v` (for all `v` on each word)
    5. `attention = sum(weighted_values)` for the current word in the input sentence.
![self-attention](nlp_images/transformer_self_attention.jpg)


#### Transformer
- solves challenge 1 & 3
- utilizes only attention mecdhanism, no RNN or CNN units
- 3 attention units: self-attention in both encoder and decoder to attend to results at previous position; encoder-decoder attention for the decoder to attend to hidden states at different positions from encoder. All attentions use multiplicative attention
- stacks attention units to form multi-head attention
- takes ordering into account
- details:
    - uses encoder-decoder paradigm
    - Encoder:
        - input embeddings (one-hot or word embeddings)
        - positional encoding 
        - 2 sublayers: *self-attention* + *feedforward network*
            - *self-attention* look at other words in the input for encoding a specific word position
            - *feedforward network* is applied independently at each position to generate output
    - Decoder:
        - 3 sublayers: *self-attention* + *encoder-decoder attention* + *feedforward network*
            - calculates a new output from *self-attention* at current time step (only attend to earlier positions, later positions are masked, this is for translation tasks)
            - calculates a new output from *encoder-decoder attention*
            - concatenate the two outputs
            - feed to a *feedforward neural network* for generating output for current time step
    - 3 types of attention: Encoder self-attention + Encoder-Decoder attention + Decoder self-attention
    - only handles fixed-length sentence, long sentences have to be broken up
- training tricks:
  - learning rate decay proprotional to $step^{-0.5}$
  - dropout at each layer before adding residual
  - label smoothing
  - auto-regressive decoding with beam search and length penalties
  - checkpoint averaging
![transformer](nlp_images/transformer_architecture.jpg)

#### BERT
*Bi-directional Encoder Representation from Transformers*

- [Good Paper Explanation](http://mlexplained.com/2019/01/07/paper-dissected-bert-pre-training-of-deep-bidirectional-transformers-for-language-understanding-explained/#more-823)
- [Good visualization](http://jalammar.github.io/illustrated-bert/)
- [Good Code Example](https://mccormickml.com/2019/07/22/BERT-fine-tuning/#advantages-of-fine-tuning)

Language modeling is an effective task for using unlabeled data to pretrain neural networks in NLP
BERT performs better when given more parameters, even on small datasets

Language modeling is essentially just predicting words in a blank. It is typically trained from left to right, but BERT randomly masked words in a sentence and predicts them (utilize both directions). Bi-LSTM or ELMO do look at both directions, but they are modeled separately, not at the same time.

![input to bert](nlp_images/bert_input.jpg)
two training tasks:

- masked language model (masked LM) training
    - words are randomly masked in sentences and the model is expected to predict a word no matter what is available in the context
    - words are also sometimes replaced with random words, but the percentage is small to avoid too much noise
    - inputs to the model should contain an attention mask that differentiates actual tokens and padded tokens
- next sentence prediction (SP) training
    - `[CLS]` is inserted at beginning of sentence pairs
    - `[CLS]` will be considered the position for sentence representation, the hidden state output from the last transfomer layer can be loaded into any classifier for other tasks
    - potentially, using `[CLS]` output only can be replaced with some pooling methods, but unclear why the author didn't do that.
    - `[SEP]` is inserted to separate sentences
- the model can be fine-tuned
    - Option 1: just add and fine-tune the final classifier layer alone
    - Option 2: the sentence representation and the final classifier can be trained jointly
    - Option 3: concatenate output from last four layers and fine-tune
    - Option 4: free up more layers/parameters to fine-tune
    - useful starting values:
        - Dropout: 0.1
        - Batch size: 32, 16
        - Optimizer: Adam
        - Learning rate: 5e-5, 3e-5, 2e-5
        - Epochs: 3, 4
    - pretraining from scratch takes \~4 days on \~16 TPUs, too much resource
- other details:
    - for long texts, if no logical pairing exists, can concatenate the sentences, but make sure it does not exceed the 512 limit. 
    - for true article paragraphs, can assume "prev - next" logic in pairing them (instead of the "Q - A" logic), therefore utilize a sliding-window way to input pairs of sentences to BERT
- notable variants: 
    - **RoBERTa**, **XLNet**, **GPT-2**

> 在很多的应用场景，attention-layer 肩负起了部分 feature-selection,featue-representation 的责任。举个例子，transfer learning with Domain-aware attention network for item recommemdation in e-commerce 中提及：不同场景的用户的行为有不同的偏好（场景是 context，价格，品牌是不同的信息），天猫用户对品牌看重，亲淘用户 focus 价格，可以通过 attention-layer 学习到不同 context 下，用户的 Attention 在哪里。在 ctr 预估中，Deep Interest Network for Click-Through Rate Prediction 出发点类似。在推荐场景中，文章 Feature Aware Multi-Head Attention 在手淘猜你喜欢排序模型中的应用 。这些都是attention 在业务场景落地的参考。


## Automatic Speech Recognition
Acoustic Model + Language Model (Hidden Markov Model /Deep NN)
Acoustic Model: 
- sound to phonetic representation
- HMMs: using labeled data, build one HMM for each sound unit (phonemes, words, etc.) for isolated input data; or build chained HMMs for continuous input data
Language Model: 
- grammar, contexts, etc. 
Challenges:  
Noise
Variability
Ambiguity
Feature Extraction:
raw audio signals: too much info + noisy info
FFT to conver signal into frequency domain
Mel Frequencey Analysis to bin frequencies and focus on ones that are relevant to human ears (lower frequencies)
Cepstral Analysis to remove individually unique info and retain information that is person-invariant. 
Results are a handful of MFCC features as a
raw numeric representation of a segment of audio data
[Speech Feature Extraction Techniques: A Review](http://www.ijcsmc.com/docs/papers/March2015/V4I3201545.pdf)

[Mel-Frequency Cepstral Coefficients and Analysis](http://www.speech.cs.cmu.edu/15-492/slides/03_mfcc.pdf)


---  
my view on the history of NLP: (follows how a human learns)
**learn by**

- imitation/recognition (key words/regex)
- pieces/enumeration (n-gram/tf-idf)
- memorization (RNN/LSTM/GRU)
- summarization (Attention/word2vec)
- comprehension (transformer + attention)
- fusion (yet to come)
