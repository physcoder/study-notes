# Installation
## Conda Install
create a conda environment and activate it
install tensorflow in it: conda install -c conda-forge tensorflow
## Repo Structure
TensorFlow is an organization on github, under which there are multiple repositories, most common ones are `models`, `tensorflow`, `tensorboard'`


# API
lowest level API: TensorFlow Core (full programming control)
higher level APIs: on top of the Core (easier to use), e.g. `tensorflow.contrib.learn`, `tensorflow.contrib.slim`

# Versions
be careful about using pre-trained models downloaded online. If the model was generated using a newer version of TF, there might be incompatibilities. Also, protoc vs. (protobuf for python) versions should match.

# Tensor
Central unit of data in TensorFlow.
It is a set of primitive values grouped into an array of any dimensions.
Tensor has a *rank* (number of dimensions), and a *shape* (lengths in each dimension)
```
3 is of rank 0, shape []
[2,3] is of rank 1, shape [2]
```

# Computational Graph
A series of operations arranged into a graph of nodes.  
Each operation is a node.  
Each node takes zero or more tensors as input and produces a tensor as output.  
Each TensorFlow program consists of two steps: (1) build the graph, (2) run the graph.
The running part is completely outside of python, for speed and efficiency.
`Session` : created to use `session.run(graph [, feed_dict])` to actually evaluate the graph  
`TensorBoard`: can visualize the graph  
`placeholder`: created to hold a future input parameter, it does not change in training. Can be replaced by actual values via `feed_dict`
`Variable`: allows adding trainable parameter to a graph, it is trained(changed) during training.  
`constant`: once the value is set, can't change anymore.  
Other operations: `tf.add()`, `tf.sum()`, `tf.square()`, `tf.assign()`, `tf.global_variables_initializer()`, etc.


# tf.contrib.learn
A high level TF library, resembles the `sklearn` library.
It has predefined estimators (models), input/output functions for data
It also supports customized model. Define a model function returning a `tf.contrib.learn.ModelFnOps` object, and pass it in `tf.contrib.learn.Estimator`


# Workflow
prepare the data, using `numpy` or `TFRecords` if the data is large
construct the computation graph
train the model by loading in the data into a `TF.session`
evaluate the model

dropout in training; add dropout somewhere in the conv layers.
