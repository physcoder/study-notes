# Rule of Thumb

## NN may not always work out
if a decent starting point along with some tuning efforts does not give a decent baseline performance, might want to revisit the data, approach, or model.

## Start Simple
- images: start with Inception/VGG-like architecture
- sequences: start with LSTM with one hidden layer
- others: fully connected NN with two hidden layers
- mixed: separate models to construct features (lstm+conv), then concatenate, add fully connected layers

## Number of layers
- start with two hidden layers
- more layers means more non-linearity
- if convinced NN is the approach, can start deep to overfit, then trim down by binary search till it underfits

## Number of nodes
- use 2^n nodes
- reduce/increase by half from layer to layer
- first layer shouldn't need nodes more than the input dimension, can try `d/2`

## Output Layer
- regression/binary classification: one node
- multiclass/multi-response: number of classes/responses
- use *sigmoid* for binary classification; use *softmax* for multicalss classification; use *linear* for regression

## Dropout
- usually no or p<0.2 dropout at input layer
- don't use p>0.5; typically start with 0.5

## Batch Normalization
- Typically replaces Dropout in modern models
- it commutes with pooling, so the order largely affects the speed only
- BN does not commute with ReLU, people differe in the order between BN and Activation

## Activation
- Monotonic activation functions commute with (max- or average-)pooling. This means that the order does not matter
- `RELU` for FC and Conv models, most common
- `tanh` for LSTM-like models
- `GELU`???

## Weight initilization
- `normal: N(0, sqrt(2/n_input))` for `RELU`
- `truncated normal: N(0, sqrt(2/(n_input + m_output)) ` for `tanh`
- others: He-normal, Xavier, etc.
- change according to weight or activation value histograms in training (vanishing, exploding, etc.)

## Loss
- `binary_crossentropy` for binary classification
- `categorical_crossentropy` for multiclass classification, if labels are one-hot encoded
- `sparse_categorical_crossentropy` for multiclass classification, if labels are integers
- `mse` for regression 
- loss values:
    - should be small typically, roughtly `<=2.0`;
    - for cross entropy, the initial loss should be `~=-ln(1/NUM_CLASSES)` 

## Epochs
- start with `20`
- observe trends in loss (decreasing, plateau, oscillating, etc.) 

## Batch Size
- start with 16, change by power of 2
- increase when model overfits
- increase (e.g. 128) for imbalanced classes

## Learning rate
- usually around `0.01` or `0.001`
- increase if using batch normalization or adam/momentum/RMSprop optimizers
- decrease if loss is oscillating

## Data Prep
- normalize data by scaling
- try to balance the classes

## Overfitting (in the order of try first to try later)
- Add more data
- Add normalization (batch norm, layer norm)
- Increase regularization (dropout, L2, weight decay)
- Error analysis
- Choose a different model
- Tune parameters
- NOT recommended: early stopping, remove features, reduce model size

## Visualization for debugging
Visualization is the best way to ensure an NN is learning properly.
- loss vs. epoch
- Histogram of weights, gradients (exploding vs. diminishing)
- Ratio of weight-magnitudes to weight-updates (too low, `<1e-4`, means not learning effectively)

## Other implementation details:
- turn off bias when using batchnorm
- scale output in testing mode for dropout layers
- too much regularization results in loss plateau

