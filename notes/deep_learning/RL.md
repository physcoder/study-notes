# Reinforcement Learning
RL learns optimal policy from Environment-Agent interactions. The state of a system can be characterized by (State, Action, Rewards).

## Settings
- **State**: the time-dependent state the environment is in after each action $(S_0, S_1, S_2, ...)$
- **Action**: the agent's time-dependent action after observing the state $(A_0, A_1, A_2, ...)$
- **Rewards**: the feedback from the envrionment to the agent after taking an action, rewards start after first action is taken $(R_1, R_2, R_3, ...)$
- **Goal**: maximize expected cumulative rewards.

### Tasks
- *episodic tasks*:  where there is a well defined ending point of interactions, e.g. games
- *continues tasks*: where the interaction can go on forever, wi thout a definitive ending point, e.g. stock trading

### Rewards
Reward definition should account for many competing aspects of the task for the agent to balance, e.g. walking: walk fast + walk forward + walk smoothly + walk longer.

- cumulative future rewards  
  At any timestamp $t$, the agent chooses action $A_t$ to maximize cumulative **future rewards** $G_t = R_{t+1} + R_{t+2} + R_{t+3} + ...$, because the past has already happend and fixed. However, the future is unknown, so the agent relies on some predictions of future rewards based on information collected so far.

- discounted return  
  immediate rewards are more relevant and likely, rewards far into the future are less known and reliable. So the cumulative rewards is then: $G_t = R_{t+1} + \gamma R_{t+2} + \gamma^2 R_{t+3} + \gamma^3 R_{t+4}...$

### MDP
#### *Markov Decision Process*:  
Given the state space $S$, action space $A$, associated rewards $R$, and transition probabilities $p(s', r|s, a)$, a MDP can be constructed to fully characterize the interactions between agent and environment. This is the foundational framework of RL.

#### *Policy*:  
Policy determines what actions to take given the state of the environment. The goal of RL is to learn the optimal policy under a specified action space, state space, and rewards.

- deterministic policy, maps state to action: $\pi: S \mapsto A$
- stochastic policy, maps state to all actions with some probability: $\pi: S \times A \mapsto [0, 1]$, usually written as $\pi(a|s)$

#### *State-value function*, $v_{\pi}(s)$:
- Definition   
  state-value function is policy-dependent, which gives the expected return of starting in the current state and following the given policy for all future actions.
  ![state-value function](./images/state_value_function.PNG)

- Recursion property (**Bellman Equation**)  
  The state-value function can be calculated recursively from the end back to the start by Bellman Expectation Equation.  
  The Bellman Expectation Equation can also apply to action-value functions as well.
![bellman expectation equation](./images/bellman_equation.PNG)

#### *Optimal Policy*, $\pi_*$
- policy evaluation  
  one policy is better than another if and only if its corresponding state-value function is bigger in all states than that of the other policy, i.e.
$$ \pi > \pi' \iff v_{\pi}(s) \ge v_{\pi'} \forall s \in S $$

- optimal policy  
  policies may not be comparable, given the above definition. But there is at least one that satisfies the criterion against all policies. The optimal policy may not be unique.

#### *Action-value function*, $q_{\pi}(s,a)$:
- Definition   
  action-value function is policy-dependent, which gives the expected return of taking a particular action in the current state and then following the given policy for all future actions.

- Why defining this function?  
  It serves as a way to estimate optimal state-value functions (e.g. picking the largest action-value at each state) to come up with optimal policies

  From action-value to optimal policies, the agent needs to:

  1. interacts with the environment
  2. estimates/predicts optimal action-value functions
  3. constructs state-value functions for an optimal policy from the action-value functions 
  ![action-value and state-value definitions](./images/action_value_function.png)

## Approach 1 - Dynamic Programming
**Assumption**:   

agent has full information of the envrionment (how rewards and states are defined and transitioned), an easier setting than Reinforcement Learning

**Workflow**:   

1. policy evalution: use Bellman equation to iteratively estimate state-value functions
2. policy improvement: based on current estimate, obtain a better policy than current, then iterate

### Part 1 - Iterative policy evaluation
Given a finite MDP, by using the bellman equation, the state-value functions of a policy can be written into a system of equations, where each state has a corresponding equation. The system of equations can be solved in an interative way by starting with a random guess and ending in meeting convergence criterions. 

- Input: MDP, policy, zero-initialized state-value functions, convergence criterion
- Output: (converged) state-value functions corresponding to that policy

Once state-value functions are obtained, action-value functions can be calculated from them, by:
$$q_{\pi}(a, s) = \sum_{s', r} p(s', r|s, a) (r + \gamma v_{\pi}(s'))$$
![](images/iterative_policy_evaluation_1.png)
![](images/iterative_policy_evaluation_2.png)

### Part 2 - Policy Improvement
improvement can be done in two steps: 

1. obtain the action-value functions from state-value functions; 
2. construct a better policy by taking the action that maximizes the action-value function at each state (**greedy policy**)
![](images/policy_improvement.png)

### Part 3 - Policy Iteration
combining part1 and part2 to iteratively converge to the optimal policy
![](images/policy_iteration.png)

### Part 4 - Value Iteration
Removing redundancies in the Policy Iteration framework by combining policy evaluation and policy improvement into one operation, Value Iteration only updates the state-value functions with a one last step to extract from $v_(s)$ the optimal policy $\pi(s)$.
![](images/value_iteration.png)


## Approach 2 - Monte Carlo Method
**Assumption**:  
Agent knows nothing about the envioronment and needs to learn from interactions.

**Workflow**:  
Given a policy, the agent follows it in a series of independent episodes in the envioronment, and then uses the episodes to estimate the value functions.

After all the episodes or each episode, a control algorithm is used to improve the policy. Then iterate the process.

MC method only works with episodic tasks

### Part 1 - Estiamate Value Functions
Value functions can be estimated by counting the rewards of *visited* states or state-action pairs. Different methods exist, e.g. first-visit method, every-visit method, etc.

Iteratively, the value functions can be estimated over many episodes by a running mean algorithm

### Part 2 - Policy Improvement
One can act similarly to the greedy algorithm in dynamic programming, taking actions that will maximize the return. However, because the environment is unknown to the agent (unlike in dynamic programming), the agent has to balance between exploitation and exploration. So the $\epsilon-greedy$ algorithm is used for policy improvement. To make *more* use of (exploit) the knowledge learned over time, $\epsilon$ should slowly decay to zero.
![](images/mc_policy_improvement.png)


## Approach 3 - Temporal Difference Learning
**Assumption**:  
Agent knows nothing about the envioronment and needs to learn from interactions.

**Main Idea**:
Agent doesn't wait till the end of an episode to collect the rewards and update the value functions, but instead tries to predict the probability of winning eventually at every time step and amends to the current policy, that is using the temporal difference between time steps.

TD learning works with both continuous and episodic tasks

### Part 1 - TD Prediction
the update step of value functions in the Monte Carlo method has to be modified without using the eventual rewards, but the new rewards at each new time step. The update step in the image below can be rewritten as follows, to see the balance between exploration vs. exploitation, controlled by $\alpha$:
$$ V(s_t) = (1-\alpha) V(s_t) + \alpha (R_{t+1} + \gamma V(s_{t+1})) $$
![](images/td_state_value_update_1.png)

action values can be updated in the same fashion, by replacing $V$ with $Q$, that is, updates are performed after each action, rather than state.
![](images/td_action_value_update.png)

### Part 2 - TD Control
#### Sarsa(0) a.k.a Sarsa
Using similar $\epsilon-greedy(Q)$ method in MC, the optimal policy can be guarateed by updating the policy after each action.
The name `Sarsa` means a tuple of (s, a, r, s, a) is used for each update. 
This method is on-policy.
![](images/sarsa.png)

#### Sarsamax a.k.a. Q-Learning
Sarsamax updates the policy before the agent taking a new action, and the action used in the update step is one derived from the greedy policy. The next action after the update is derived from the $\epsilon$-greedy policy. 
Sarsamax directly estimates the optimal policy at each step, rather than just the $\epsilon$-greedy policy. 
This method is off-policy.
![](images/sarsamax_2.png)
![](images/sarsamax_1.png)

#### Expected Sarsa
This method goes one step further to take into account all possible actions and corresponding Q values when making the update. This method is on-policy.
![](images/sarsa_expected.png)

#### Comparison
Sarsa and Expected Sarsa are both on-policy, while sarsamax(Q-Learining) is off-policy. On-policy methods tend to have better online performances (collect more rewards in each episode) but learns a sub-optimal safe policy. Whereas, off-policy has worse online performance, but learns the potimal policy.

*On-policy* vs. *off-policy* methods:  

- On-policy learning:  estimate the value functions of a policy while using it for control (taking actions)
- Off-policy learning: policy used to generate behaviors may be unrelated to the policy being evaluated and improved


## Approach 3 - Deep Q-Learning
neural networks as value function approximators; but the problem is how to define the loss, without knowing the underlying true value function forms

$\hat{q}(s, a, w)$ as the approximation of actual $q$, the weight update step can be done by using either the cumulative rewards after each episode (in MC setting), or use the next reward and next value function (in Sarsa setting), or use the greedy action values (in q-learning setting) as the (approximated) target.