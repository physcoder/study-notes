## RNN

- RNN can maintain short term memories. RNN can be very carefully tuned to remember long term histories for a specific problem.

- However, due to the vanishing gradient issue, it typically does not remember information transmitted from long ago (the weight updating equation is usually a product of learning rate and error term from previous layer and input to that layer, so for a particular layer, the error term would be a product of all previous errors; for typical activations like sigmoid, their derivative will multiple such small values multiple times as we go towards the starting layer, thus the gradient vanishes)


## LSTM
- Great explanation of the structure of LSTM (cell state, forget gate, input gate, output gate)
[link](http://colah.github.io/posts/2015-08-Understanding-LSTMs/)

- The core idea is that the current output should be dependent on the previous cell state, the previous hidden state, the input at the current step; selectively remembered/forgotten

- the input to LSTM is a 3D array of (samples, lookback, features)

### LSTM in Keras
- `return_sequences` is for returning the outputs of the hidden states (h) **at each time step**, otherwise it will only return only one hidden state. It will be needed for stacking LSTM layers
- `return_state` is for returning the cell states (c) and the hidden states (h) **at the last time step**. Generally this is not needed, but for encoder-decoder models where the next layer requires its cell states initilized to the final cell state of the previous layer
- `return_sequences` and `return_state` can be used together to access both h and c sequences