## CNN
- Designed for data that can be transformed into 2D arrays, like images, sound spectrum with time, etc.
- Better than MLP in that it considers the 2D local connectivities and also uses much less parameters due to weight sharing within filters.

####Structure:  
Convolutional layer + Fully connected layer
convolution: kernel size + stride + padding

####Workflow:   
1. Input image ->> 
2. Convolution with different filter/templates (typically 3x3) ->> 
3. Linear Rectified Units (turn negative into zero) ->> 
4. Max pooling (record the max value at each window location) ->> 
5. Repeat the process, till forming a single column feature vector ->> 
6. Dense layers ->> 
7. Output class votes for classification

####Implementation
- Loss: cross entropy

- Dropout:    
  - In training, for each hidden unit, assign it as zero with probability of p (usually 0.5). This is essentially removing units at random during the back propagation; 
  - In test, restore all hidden units, but their weights are p*w (mathematically, the dropout network is a scaled version of the regular network, assuming the dropout follows Bernouli distribution). 
  - For one layer NN with p=0.5, it can be shown as equivalent to a geometric average of all possible NN configurations with different masks, so it is like an ensembling method. This is method is slower.
  - Dropout at input layer should be kept low p<0.2; intermediate layers can go up to p=0.5; it is not advised to go p>0.5, b/c mathematically p=0.5 gives the largets regularization (p(1-p) term in loss), going higher does not increase regularization, but cuts out more connections

- LRU: For introducing non-linearity; alternatives: leaky RELU, softmax, tanh, GELU
- Pooling: For compression, invariance to local translation; nowadays deprecated;
  - window size + stride
  - max pooling
  - average pooling
  - global pooling: down sample to just one value; average or max; sometimes used as alternative to dense layers

- Weight initialization
  - uniform distribution: `[-1/sqrt(N), 1/sqrt(N)]` (pos and neg values)
  - normal distribution: `N(0, 0.1)`, usually slightly better than uniform
  - truncated normal distribution: benefits show on larger networks

- Batch normalization: 
  - applied right before activation (applied to Wx+b)
  - reduces internal covariate shift; enables higher learning rate; regularizes the model
  - dropout can be removed or reduced in strength

- Image augumentation

- Transfer Learning:
  - small data & different domain: slice off pre-trained layers near the beginning (only use low level features) and replace the fully connected layer at the end; keep the other weights the same

####Architectures
- VGGNet: 3x3 conv layers
- ResNet: skip layers for gradient propagation and combining features at different levels
- Inception v3: different-scale conv filters; auxilliary classifier

####Autoencoder
- not as good as traditional methods at compression
- more used for image denoising (cleaner at reconstruction) and dimension reduction (extract mid layer)
- convolutional autoencoder typically works better


