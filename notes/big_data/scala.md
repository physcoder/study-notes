# Scala
It is object-oriented and static typing, but one does not need to specify type and certainly does not need to repeat it. It runs on JVM. Everything is an object. [Quick Guide](https://www.tutorialspoint.com/scala/scala_quick_guide.htm)

Scala compiler compiles the code into Java Byte Code, and then the `scala` command executes it. Scala code can be run in interactive mode or script mode:
```
scalac script1.scala
scala script1
```
Name of the program file should exactly match the object name.

Scala program processing starts from the `main()` method which is a **mandatory** part of every Scala Program
```
object ObjectName {
  def main(args: Array[String]) {
  }
}
```


## Syntax
### Comments
`/* */` and `//`

### Packages
#### declare
`package package.name`
#### import
`import package.name` and `import package.name.{obj1,obj2}` and `import package.name._`

### Data Types
the same data types as in Java

numeric literals are by default `Int` or `Double`

boolean literals, `true` and `false`

character literal is a quoted single character

string literal is a sequence of characters in *double* quotes

null

tuple is immutable `val x=('a','b','c')`, elements are accessed by `._1`, `._2`, etc

### Variables
Scala differentiates a value (constant) from a variable (mutable) by using `val` and `var`.  
General format is `val or val VariableName : DataType = [Initial Value]`.  
Scala can infer a data type from the initial value, so sometimes no need to specify the type.
Method parameters are always immutable `val`
For example:
```
var myVar : String = "Foo";
val myVal : String = "Bar";
var myVar = 1;
var (a,b) = (1,2)
val (a,b) = (3,4)
```

# Built-in Collections
`List`
`Vector`: `val a = Vector(1,2,3); a(0);`
`Map`: `val a = Map(1 -> "one", 2 -> "two", 3 -> "three"); a(1);`
`Set`
Default collections are immutable, but mutable collections are also available
Same API applies to different collection types: `a(i)` for indexing, `length()`, `:+` for append, `+:` for prepend, `contains`, `indexOf`, `sorted`, `foreach`
`Array`: mutable


### Objects and Classes
Class name serves as constructor      
Instantiate with `new`    
Inherit with `extends`  
methods and fields are by default public   
`trait`, like the `abstract class` in Java, is used to define object types by specifying the signature of the supported methods, but methods can remain un-implemented. A class can use as many traits as desired.  

```
class Point(val xc : Int, val yc : Int){
  var x : Int = xc
  var y : Int = yc

  def move(dx : Int, dy : Int){
    x = x + dx
    y = y + dy
    println('New x Location: ' + x)
    println('New y Location: ' + y)
  }
}
```

### Flow Control
```
if(Boolean_expression){
   //Executes when the Boolean expression is true
} else if(Boolean_expression){
   //Executes when the Boolean expression is false
} else {
   //Executes when the Boolean expression is false
}

```

```
while(Boolean_expression){
  //statements
}
```

```
for ( a <- 1 to 10 ){
   println( "Value of a: " + a );
}

for ( a <- Vector(1,2,3,4,5) ){
   println( "Value of a: " + a );
}

for ( a <- 1 until 10 ){
   println( "Value of a: " + a);
}
```


### Functions
#### Function basics
A function that does not return anything can use `Unit` as return type.
`()` and `{}` are not needed if no params and only one statement involved.  
Anonymous function
Nested function
```
def functionName ([list of parameters]) : [return type] = {
   function body
   return [expr]
}

def f( x : (String, Int), y : Double ) {}

def f = "this is a function";
```
#### Functional iteration
