# Spark
## Overview
Apache Spark is written in Scala, but supports Scala, Java, Python
- First, a RDD needs to be created (from HDFS or other input)
- Operations includes **actions** and **transformations**.
- **actions** perform some computations and return values; while **transformations** involve certain mappings and return pointers to new RDDs
- two fundamental abstractions, **RDD** and **Shared Variables** (broadcast variables and accumulators)
- a good guide with examples: [Spark Programming Guide](http://spark.apache.org/docs/latest/programming-guide.html)


## RDD, DataFrame, Dataset
- RDD is the primary data abstraction, which is an immutable distributed collections of elements of the data
- DataFrame and Dataset are both built upon RDD, but data can be moved in between structures by simple API calls
- Working with RDD provides more low level manipulation and control. It is good for functional programming and unstructured data.
- DataFrame and Dataset structures the data into named columns with optimized operations
- DataFrame and Dataset will have unified API in Spark 2.0; the API has untyped (DataFrame) and strongly-typed (Dataset) calls. In Python, only DataFrame exists.
- DataFrame is just a collection of `Dataset[Row]`, where Row is some undefined generic
- Dataset is a collection of `Dataset[T]`, as dictated by a specific `case class` defined in Scala or Java. But only DataFrame is available in R/Python, as they are naturally untyped language.
- DataFrame and Dataset provides high-level operations, much faster speed, and efficient space-management. They are good for structured/ semi-structured data


## Download
- [download link](http://spark.apache.org/downloads.html)
- start by `cd /path/to/spark`, invoke `bin/pyspark` or `bin/spark-shell` this is the interactive shell.
- type`:command` for help and other import interactions in shell


## Initializing Spark
### < Spark 2.0
RDD is the main API and manipulated by context APIs, so for every other API, a different context need to be instantiated (SQLContext, HiveContext, etc). The main entry is A `SparkContext` object created first. In order to create it, a `SparkConf` object needs to be created beforehand that contains information about the application (application name, cluster URL, etc.). Only one `SparkContext` object per active JVM.

### >= Spark 2.0
DataFrame and DataSet are the main APIs. `SparkSession` is the entry point, which is a combination of SQLContext, HiveContext, and StreamingContext. SparkSession has an internal SparkContext.


## Creating an RDD
- parallelizing an existing collection in the program by `sc.parallelize()`
- referencing an external dataset stored in a Hadoop-supported format like local filesystem and Hadoop InputFormat (HDFS, HBase, ...)


## RDD Operations
- **transformations** like `map` will not execute until an **action** like `reduce` is needed and results must be returned. So **transformations** are lazy.
- each transformed RDD may be recomputed each time an **action** is run on it, but `cache()` or `persist()` can be used to keep the RDD in memory for faster and frequent access
- *chained operations* is much preferred, due to the laziness of the transformations


## Passing functions to each element of RDD
- Usually anonymous functions
- Functions should not depend on external variables or data.
- Define function inside a singleton object. It not advised to reference a function/variable that is inside a class object
- Use **accumulators** when counter updating is desired (otherwise, the task is split onto different nodes and so are the variables, the shared variables may not be updated correctly)
- Certain functions such as `sortByKey()` and `reduceByKey()` are only available to key-value paired RDDs
- **broadcast variables** are created by `val broadcastVar = sc.broadcast(v); broadcastVar.value;`;
- **accumulator variables** are created by `val accum = sc.LongAccumulator()` or `DoubleAccumulator()` (numeric)


## Printing data
`.foreach(println())` does not work with clusters since each node will print to its own stdout.

Use `collect()` before `println`, but may cause memory overflow as all results are collected to one machine.

Use `.take(n).foreach(println())` to print a fraction of the data


## MLlib
MLlib has two separate packages, `spark.mllib` and `spark.ml`. `spark.mllib` is built with API over the original RDDs and is now in maintenance mode; `spark.ml` is built over DataFrames, which should be the recommended approach.

Be careful about which, `ml` or `mllib`, is imported, as they require different input types (slightly different APIs).
To convert code from RDD-based API to DataFrame-based API, functions need to be imported from `ml`; RDDs need to be converted using `toDF()`; data within DataFrame also need to use `ml`-defined data types, such as `Vector` or `Matrix` in `linalg`, using `asML()` in a `.map()` transformation

Use `.explainParams()` for documentation on a function.

Use `Pipeline` from `ML`, to establish full workflow of data science. It can then be saved and read in in a production env so deployment becomes easy.

Different models may expect different input types. When needed, use `ml.linalg.Vectors.` to convert to necessary data types.

## Spark SQL
Spark SQL queries return `DataFrame`s as results. each entry is an `pyspark.sql.Row` type, which is organized like an dictionary in python. SQL can be run over DataFrames registered as Tables.

### Spark SQL < 2.0
```
from pyspark.sql import SQLContext
sqlcontext = SQLContext(sc)
data_df = sqlcontext.createDataFrame(data_RDD) # data_RDD must be RDD[Row]
sqlContext.registerDataFrameAsTable(data_df, 'table1')
sqlContext.sql("select * from table1")
```

### Spark SQL 2.0
```
from pyspark.sql import SparkSession
spark = SparkSession.builder.appName('the Name').getOrCreate()
df = spark.read.json("examples/src/main/resources/people.json")
df.show()
df.select('name').show()
df.filter(df['name']>2).show()
df.groupby("age").count()

df.createOrReplaceTempView("people")
result_df = spark.sql("select * from people")
```

### Load/Save data
`parquet` is the data source by default. Data source can be specified by `format=` in load.

Parquet files are self-describing, so the schema is preserved when saving/loading

```
df = spark.read.load("/path/to/file.parquet")
df.write.save("/path/to/file.parquet")
```

### Run SQL directly on a file
```
spark.sql("select * from parquet.`path/to/file`")
```

## PySpark
### Workflow
Create an RDD from file or in memory (`sc.parallelize()`), perform actions/transformations, collect/return the results
- from pyspark import SparkContext
- sc = SparkContext()
- rddread = sc.textFile('path/to/file')
- rddread.collect()
- rddread.first(); rddread.take(n); rddread.takeSample(withReplacement, n, [seed])
- rddread.count()
- *(key, value) pairs* are formed by `map` to tuples

### Spark Actions             
- collect()     : collect from nodes and translates into Python objects (or display them in stdout in scala)              
- count()
- reduce( function )    : reduces all elements in the RDD
- first()
- take(n)       : return first n lines
- tail()    
- takeSample( withReplacement, num, [seed] ): take samples
- foreach()

### Spark Transformations
- map( function ) : one of the most important method; map each line element of the RDD data; returns a sequence of the same length as the original data
- flatMap()       : flattens the results of `map`
- filter()        : selects like SQL WHERE
- sortByKey()     : works on RDDs of type (K,V), can pass `keyfunc` to extract the relevant piece for comparison in key
- reduceByKey()   : works on RDDs of type (K,V) and performs reduce calculations on the Vs of each K
- countByKey()    : works on RDDs of type (K,V) and returns the count of each key as (K,int)
- sample()
- union()         : RDD1.union(RDD2)
- intersection()  : RDD1.intersection(RDD2), work well only when each element of the RDD is a string
- distinct()      : RDD1.distinct(), work well only when each element of the RDD is a string
- join()          : when operated on RDD of (K,V) and (K,W), RDD of (K, (V,W)) will be returned.

### Submit Spark Applications
`spark-submit` is used to submit written scripts to spark clusters. Uniform API means no further configuration needed. But one can specify which cluster to use with `-master`, and useful spark configuration properties with `-conf`
