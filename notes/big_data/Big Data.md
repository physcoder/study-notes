# Big Data

## Hadoop

### Hadoop Cluster
Hadopp cluster usually has a few *master nodes* which controls and manages the overall system, with many *slave nodes* that stores the data and processes them. The cluster is designed to handle large stationary data, so average file size is usually above 500MB. 

Slave nodes stores the raw data while master nodes stores mostly the metadata about each the states of the system (blocks, slave nodes, etc). slave nodes regularly reports to master nodes.

Both **master nodes** and **slave nodes** are cluster service nodes, there are also **edge nodes**, which is the gateway between the cluster and end users. Edge ndoes usually contain management tools, data transfer staging tools, and applications. Tools like Oozie, Pig, Hue, Sqoop run well there. This way less deployments are on the service nodes, so **Name Node** and **Data Node** can take up more resources as needed. 

### Hadoop Ecosystem
- **Hive**, a distributed data warehouse for data stored in HDFS, and also has a query language HIVEQL; a platform used to develop SQL type scripts to do MapReduce operations. 2 basic components: HIVE cmd and JDBC/ODBC driver
- **Pig**, a platform for the analysis of very large datasets that run on HDFS; a procedural language platform (Pig Latin) used to develop a script for MapReduce operations. Just another alternative to Hive, but more scripting
- **Hue**, a GUI tool for browsing files and issuing HIV and PIG queries
- **Hbase**, a distributed, non-relational, columnar database, acting as the data store for Hadoop. It supports all types of data.
- **Sqoop**, a data ingestion tool; an efficient tool to transfer large datasets between relational database and HDFS (import and export).
- **Flume**, a data insgestion tool; mainly used for unstructured data, streaming from various sources, only imports data
- **Oozie**, a schedular for jobs/applications
- **YARN**, resource management in commandline 

### MapReduce
- the core of Hadoop; a programming framework for processing large sets of data on a Hadoop Cluster, especially good for textual-log-processing
- workflow: **Map** step -> **Intermediate** step -> **Reduce** step

- There are various ways to execute MapReduce operations:
    - The traditional approach using Java MapReduce program for structured, semi-structured, and unstructured data.
    - The scripting approach for MapReduce to process structured and semi structured data using Pig.
    - The Hive Query Language (HiveQL or HQL) for MapReduce to process structured data using Hive.

- explanation in details:
    - always take key-value pairs as input and output
    - Map step takes an input (key, value) pair, usually of the form (doc_name, texts), and transform into a list of (key, value) pairs, such as in the form of (word, freq)
    - Intermediate step **collect** such pairs from all nodes and **sort** them by key and **shuffle** to load pairs that have keys within a certain range into the same node, output is in the form of (key, list{values}); intermediate steps includes: **sort**, **shuffle**, **partition**
    - Reduce step aggregates the pairs together with the same key and output the final value combining all the previous results into also a (key, list{value}), the list usually has length one
    - MapReduce can also declare a `setup()` method that allocates some expensive resources before any input processing, such as database connection; then a `cleanup()` method that releases the resource afterwards.
- heavily uses I/O to the hard disk, but uses very little memory

- the framework is written in **Java**, and not easy to debug and use interactively. Other tools are built for easy-to-use API and interactive command lines, high level languages such as Hive (make use of SQL query forms), Pig, Impala, and Cascading

### HDFS:
- Hadoop File System; files are stored distributedly in blocks. Each block will have 3 copies across the slave nodes. Default block size is 64MB physically.
- Two Core components: **NameNode** and **DataNode**. Clients always interact with **NameNode**, which only stores "table of contents" and distributes requests to **DataNode**. 
- 'Write Once, Read Often' model. Once loaded, data file can only be appended, removed, renamed.
- Every step in MapReduce output the results to HDFS format;
- An HDFS block splits data into physical divisions while InputSplit splits input files logically; Split is the logical representation of data present in Block; Map reads data from Block through splits, so their sizes can be different, depending on the computing resources available;

### hadoop commands for managing files
- Commandline interface, invoked by `bin/hadoop`.
- `hadoop [COMMAND] [-GENERIC_OPTIONS] [COMMAND_OPTIONS]`.
    - command is a string consists of scheme and resource location. scheme can be `hdfs` for HDFS and `file` for local filesystem. (`fs` is replaced with 'hdfs dfs' in 2.0; configuration file can specify default options)
    - `-put` and `-get`, copy local file into HDFS or the other way around
    - `-mkdir`, `-rm`, `-ls`, `-stat`, `-text`, and so on



## Spark
- useful link: http://blog.cloudera.com/blog/2014/09/how-to-translate-from-mapreduce-to-apache-spark/
- a *fast* and *general-purpose* big data computing framework
- the framework is written in **Scala**, but has APIs for different languages
- a completely separate platform from Hadoop, but can run MapReduce to make use of its HDFS formats
- Spark uses in-memory caching; it loads entire data into memory and intelligently distribute them onto nodes
- User writes the map and reduce code while Spark figures out how to distribute
- Can do more than data processing, but also machine learning and interactive visualization
- Designed for efficient execution of iterative algorithms such as machine learning
- Much faster speed than Hadoop, but requires a lot of memory
- **MLib** is the machine learning library; **RDD** is the primary data structure; **SparkSQL** for SQL; **GraphX** for manipulating graphs
- Features:
    - speed; flexibility
    - integration of HDFS
    - supports RDD
    - accessible via an interactive command line shell (Scala)
- RDD:
    - Resilient Distributed Datasets
    - a collection of elements that can be used to store list of tuples, dictionaries, or lists.
    - data in RDD are *immutable* and *distributed* across different machines, can be *cached in memory*
    - it is the Spark representation of a dataset in the framework, just like DataFrame in Pandas
    - low level functionality and control, fault-tolerant
    - API: good for unstructured data, functional programming processing, no requirement for tabular formatting (columns, attributes)
- Dataset:
    - Built on top of RDD, also a immutable distributed collection of data, but organized in *named columns*
    - Dataset has *strongly-typed* API; DataFrame has *untyped* API. (only DataFrame in Python API)
    - fast and memory-efficient, offers structured view of data and analysis

## Interview Questions
- https://intellipaat.com/interview-question/map-reduce-interview-questions/
- http://www.edureka.co/blog/interview-questions/top-apache-spark-interview-questions-2016/


## Spark in Python
Spark applications are managed and distributed by `SparkContext` among the nodes.
Spark executions include:
create an RDD ->> pass functions to each element of the RDD ->> perform actions on the resulting RDD ->> computing kicks off on the cluster
```
# load pyspark on a local machine
from pyspark import  SparkContext
sc = SparkContext( 'local', 'pyspark')

# create a RDD
nums = sc.parallelize(xrange(1000000))

```
