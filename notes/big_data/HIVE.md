# HIVE
## What is HIVE
- HIVE is a Data Warehouse system designed to work on Hadoop, which queries and manages large datasets with distributed storage.
- It provides a mechanism to project structure onto the data in Hadoop and uses a SQL-like language to query them.
- One can also use HIVE to write custom MapReduce framework for sophisticated analysis

## Limitations of HIVE
- HIVE is not designed for online transaction processing, it is only used for online analytic processing
- HIVE supports *Overwriting* and *Apprehending* data, but not **deletes** or **updates**
- Sub-queries are not supported

## Why Hive is used in spite of Pig?
The following are the reasons why Hive is used in spite of Pig’s availability:
- Hive-QL is a declarative language like SQL, PigLatin is a data flow language.
- Pig: a data-flow language and environment for exploring very large datasets.
- Hive: a distributed data warehouse.

## Components of HIVE
- Metastore: Hive stores the schema of the Hive tables in a Hive Metastore
- SerDe: Serializer, Deserializer gives instructions to hive on how to process a record
- HQL: query language of HIVE with two key components, Hive Command Line and JDBC/ODBC driver


## Frequently Used Commands
build and modify the tables and other objects in the database.
```
CREATE, DROP, TRUNCATE, ALTER, SHOW, DESCRIBE
```
retrieve, store, modify, delete, insert and update data in the database.
```
INSERT [OVERWRITE, INTO]
LOAD
PARTITION BY, CLUSTER BY
DROP TABLE
```
