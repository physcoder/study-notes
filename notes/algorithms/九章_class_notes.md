# 九章算法班笔记
### Logistics
- 刷题：www.lintcode.com/ladder/164 （密码：dqs666）
- 下载课件，提前预习；下载互动课件
- 直播课只提供回放一周，8:30pm开讲
- 互动课只开放一周（2-7除外），从对应直播课播完算起
- 100题=300题的lintcode ladder （161）
- 学院福利及课件下载仅一年内有效
- 期末考试考得好有内推机会
- zoom: 82492952065; pwd: 395834
- solutions: https://www.jiuzhang.com/solutions/ + english-problem-title

### Code Style & Quality
Good coding habbits help to reduce bugs

- 逻辑、步骤尽量不要耦合在一起
- 变量命名（避免单字母）
- 避免全局变量
- 避免对输入参数进行修改（`list.sort()` vs. `sorted(list)`）
- Edgecase检测：函数参数异常；下标越界
- 函数内部不多于三层缩进
- 重复代码改写成子函数；多利用子函数减少主函数代码量；先写好主逻辑再具体写子函数，更容易看出子函数如何具体实现
- 二元运算符两边加空格（永远成立, e.g. `s[i + 1][j - 1]`）
- 空行分隔开不同的逻辑块（同逻辑不用空行）
- 多用continue少用if （在循环中，嵌套if，通过 not 变换 if 条件，可以减少嵌套）
- 尽量少用else, elif
- 判断条件尽量简短（一个 and/or 足矣），可通过子函数来精简

### Code Inteview
评价体系：

- 逻辑能力：
    - 很快想到一个solution，点出问题后能及时优化
- 编程能力
    - 代码写完 (complete solution)
    - 代码风格, 面试时不写注释，所以代码要清晰可读
    - 异常检测 (edge case)
    - bug free
    - 先写出大致框架；保持主函数代码简洁、思路清晰；再实现细节、子函数
- 沟通能力 (communication)
    - 先沟通清楚（实现方法，所需复杂度），得到肯定再开始写码（尤其在数据结构设计题中）
    - 写要安静的写，不用一边写一边解释
    - 想要大声的想，可以询问是否思路正确 (要提示)
    - 做过就说做过
- Behavior questions
    -  **Guideline**: a positive and hardworking person, strong desire to join the company
    -  why this company (culture, contribution, thoughts about products)
    -  how to balance between mentoring and my own work (part of the responsibility) 
    -  why leave current employer (career bottleneck, broaden horizon, etc.)
    -  what more questions (how to get involved more important projects, best prepare me as)    

### Exercises
- LintCode CAT (Coding Ability Test)
- Readiness:
    - ability to solve algorithms (Platinum Level)
    - bug free (2-3 submissions/problem)
    - \# of challenges solved (at least 250+)
- Optimize solution:
    - think of a brute force solution
    - identify parts that are redundant or irrelevant
    - come up with a faster solution
- Tips:
    - Avoid using indexing substring, e.g. `s[i : i + 5]`, which creates extra copy in memory
    - Greedy Algorithm 很难想对，所以基本不考，如考到也基本有其他算法可解 
        - 少数须背： https://www.jiuzhang.com/qa/2099/ 
    - 自问 follow-up 问题 （实现 or 复杂度估算）
        - 正序 vs. 逆序
        - 大于 vs. 大于等于
        - 等于 vs. 大于/ 小于
        - unique vs. all
        - count vs. actual pairs
        - 2sum vs 3sum (一般化）


### Big O Complexity
- Space complexity is relatively straightforward (just look at how many aux objects are created), but in recursion, the stack depth should also be considered
- Time complexity is crucial, understan ways to calculate (estimate # of calculations per loop)
- 利用时间复杂度反推算法：
    - 在做题过程中，如果知道题目的数据范围，我们可以通过数据范围估算时间复杂度，再根据时间复杂度估计算法。  
    - O(logn)	 二分法，倍增法，快速幂算法，辗转相除法
    - O(n)      枚举法，双指针算法，单调栈算法，KMP算法，Rabin Karp，Manacher's Algorithm
    - O(nlogn)快速排序，归并排序，堆排序	
    - O(n^2)	枚举法，动态规划，Dijkstra	
    - O(n^3)	枚举法，动态规划，Floyd	
    - O(2^n)	与组合有关的搜索问题	
    - O(n!)	与排列有关的搜索问题
- `P` vs `NP`
    -  `P`: can be solved in polynomial time (`O(n)`, `O(n^2)`, etc.)
    -  `NP`: non-deterministic polynomial, not sure if can be solved in polynomial time (`O(2^n)`, `O(n!)`, etc.)
-  common tips:
    - `O(max(n, m)) == O(n+m)`
    - `O(sum(n/i)) == O(nlogn)`
    
# Approaches
## Integer Calculations
- circular index: `i % array_length`
- mod `%`： 
    - avoid overflow: take `%` while `*`
    - `(a + b) % c = ((a % c) + (b % c)) % c`
    - `(a * b) % c = ((a % c) * (b % c)) % c`
    - `(a - b) % c = ((a % c) - (b % c)) % c`
- 1D array (nm) index `k` to 2D (n x m) array index: `(k//m, k%m)` 
- 最大/小值：`float('Inf')` or `float('-Inf')` or `sys.maxsize`

## Substrings
- common built-in functions: `isdigit(), isalpha(), lower(), upper()`
- Dynamic Programming
- Robin Karp algorithm (find pattern: map substrings to hash values, so comparisons are O(1))

## 双指针算法 (double pointer)
- 思想：usualy used to avoid using extra aux space。高频！双指针类型：相向；背向；同向
- 典型代表题目：twoSum, validPalindrome, quickSort, mergeSort, kPartitions

### 1. 排序算法（sorting）
##### quickSort
- 熟记. O(n^2) worst case (bad pivot choice); O(nlogn) on average
- 思想：divide and conquer；双指针
- use two pointers in partition
    - use `pivot = A[(left + right) / 2]`, instead of the start or the end 
    - switch out-of-pattern parir (left, right)
    - when compare pointers use `left <= right` (otherwise stuck at certain 2-element subarrays)
    - when compare element values use `A[left] < pivot` and `A[right] > pivot` (so `==` items can be moved to either side to balance the partitions)

##### mergeSort 
- 熟记. O(nlogn) in theory
- in reality, creation and deletion of aux space make the algorithm slower
- creating the auxiliary space outside mergesort function will be more efficient

### 2. 二分法 (bisection method/ binary search)
##### $O(logN)$
- **divide & conquer** vs. **bisection**: bisection reduces the problem in half iteratively while D&C processes on both halves after partitioning
- **recursion** vs. **looping**: only use recursion when specifically asked to,  or it's a DFS problem, or it's impossible to write the other way.
- 熟记Binary Search
    -  不死循环的模板以及first position 和last position的区别
    -  不仅仅用于有序数组：寻找某个值，寻找某个字符，寻找第一个/最后一个值，替代hashset作为lookup办法，均可用到
- 思想：
    - 关键在于找到**判断条件**使得数组能被分成**左右**两个不相容的组 （有答案vs没答案；可能有vs肯定有； 可能有vs肯定没有； etc.）；然后抛掉某一侧
    - 某一侧能永远能**框柱**答案，而不是定位答案
    - 也可以是在答案集上做二分，即在所有可能答案的数值区间中逐步缩小范围(e.g. MedianOfTwoSortedArrays, CopyBooks)
- 典型代表题目：first position, last position, kClosestNumber, bigsSortedArray, mountainSequence, rotatedSortedArray, smallestEnclosingRectangle, copyBooks, kthLargest

##### 其他 $< O(n)$ 的算法
- $O(logN)$: 快速幂算法、辗转相除法 (求幂指数，求最大公约数，etc.）
- $O(\sqrt{N})$: 质因数分解, 分块检索法（countOfSmallerNumberBeforeItself）

### 3. 同向双指针法
- 思想：
    - `(i, j)` 基于问题特点，`j`只能向一边走，没有回头路；达到一定判定条件，只能同向移动`i`, 而`j`可以保持不动。
    - 通常定义`j`为需要找的区间后面一个位置（写code前定义好`i, j`含义）
    - 通常题目特点：找数组区段，找substring，要求O(n)，某些链表检测
- 典型代表题目：characterReplacement, slidingWindow, deDuplicate, dataStreamMedian, topKLargestNumbers, linkedListCycle II, intersection of linkeListCycle
- 熟记模板：
     
    ```python
    j = 0
    for i in range(len(s)):
        while (j < len(s)) and some_condition:
            some_steps
            j += 1
    ```
- 链表中的快慢双指针算法：算中点，检测循环，以及判断循环入口和长度


### 4. 相向双指针法
- 思想：参考quicksort的partition算法
- 典型题目：twoSum的各种变形，MoveZeros, KPartitions, sortColors, Interleaving +/- numbers, 

## BFS - 宽度优先搜索
适用场景：

- 层次遍历：（二叉树先序遍历vs层次遍历）先序遍历通常使用递归方式来实现，即使使用非递归方式，也是借助栈来实现的，所以并不适合BFS，而层次遍历因为是一层一层的遍历，所以是BFS十分擅长的；
- 等边长图的最短路径：边长一致的图是简单图，所以可以用BFS，因为BFS只适用于简单图，所以当边长不等时不可以；
- 连通块问题：（求0/1矩阵上最大的全0块）矩阵连通块也是BFS可以处理的问题，求出最大块只需要维护一个最大值即可；
- 所有方案问题：（非递归解决10个数中选5个数的所有方案）求所有方案问题，因此可以用BFS来处理，但并不是唯一的解决方式，递归也时常用到。
- 拓扑排序： BFS是最优实现方法

思想：
> typically $O(|E| + |V|)$
> ##### 按层遍历
> - 区分层：额外一层循环
> - binary tree的BFS一般用单队列queue实现，注意queue本身长度会变化，所以应以当前长度循环
> - 双向BFS适用于搜索路径长度（起点终点同时给定），理想条件下可以在`O(sqrt(n))`内找到路径长度
> 
> ##### 联通块
> - 矩阵也可看作图；需自定义邻居（边）
> - 图的BFS需要用到Hash Table，因为可能重复访问节点
>     - 何时更新visited？-> BFS的queue和hashtable应同时操作（添加node、标记访问，etc）
> - 通常需要判断下标越界和是否被visit过：`is_valid()`/ `inBound()`子函数
> - 找相邻，用循环数组：`directions=[(1, 0), (0, -1), (-1,0), (0,1)]`
> - 在矩阵上，若搜索没有特别规律可寻，DFS可能会引起stackoverflow，因为递归深度有可能很深（O(n*m)），所以一般情况都尽量选择BFS。   
> 
> ##### 所有方案问题
> - 可转化为连通块问题：每一个方案、路径看作一个节点，方案和方案之间的变化、转换关系看作是边
> - 针对题目需要画出搜索树，即从空方案开始各个方案如何有规律的从一个变换到另一个的关系图 (e.g. Subsets)
> 
> ##### Topological Sorting 
> - 只对有向图，用来找图中是否存在循环
> - 按入度（in-degree）从低到高，依次把node从图中删除（`inDegree==0`），若某一时刻图中没有入度为0的节点，而图不为空，则不可拓扑排序
> - 拓扑序不唯一（除非同时只存在一个`inDegree==0`的点），也不一定存在（e.g. 存在环），但可以存在最小（）
> - 用BFS实现更简洁

典型题目：BinaryTreeLevelOrderTraversal, FriendCircles, Knight Shortest Path II, WordLadder （可用lazy evaluation进一步优化），Word Ladder II, Topological Sorting, Subsets


## DFS - 深度优先搜索
Recursion（递归） vs. DFS（深搜） vs. Backtracking（回溯）:  
>递归是实现DFS的一种办法，但DFS也可以不用递归而用自定义的栈(stack)。一般建议利用recursion，因为利用系统自动分配的栈一般比自定义的栈更简便。回溯法就是DFS，但DFS中有可能需要手动回溯操作，即让变量回到递归之前的状态（e.g. 寻找路径）  
> DFS的递归算法相当于实现了N层嵌套循环（N根据第一层原始输入自动决定），这种理解有助于估算时间复杂度  
> 在寻找排列/组合/方案的DFS中，时常可以通过优化搜索范围，搜索顺序，使用哈希表存储访问过的节点来加速算法
>
>   - 可行性剪枝
>   - 最优性剪枝
>   - 搜索顺序优化

DFS可以通过遍历也可以通过分治实现，函数实现通常需要传入许多参数，建议先写好主函数逻辑，定义清楚递归函数的功能，在实现过程中逐步确认需要哪些参数

#### 遍历法（Taversal）
> 一个小人拿着记事本走遍所有节点，边走边记录    

实现：每一层递归函数需要传入全局记录参数（但不要用全局变量），通常不需要return（e.g. 存储所有路径的list）
特点：具体步骤有可能是基于分治思想，但是主体还是边走边记；回溯操作很重要，使得各参数回到递归之前的状态，再进入下一次递归

```python
def dfs(args, result, results):
    if  # 退出判断:
        # 更新result
        # 添加到results
        return

    for i in # 搜索范围:
        if is_valid( 检测 i ):
                # 更新参数args, result
                dfs (args, result, results)
                # 回溯，恢复args, result

    return
```

#### 分治法（Divide & Conquer）
> 分配小弟去做子任务，自己负责结果汇总    

实现：每一层递归函数需利用到下一层return结果，通常不需要传入全局变量；定义递归函数要完成的任务很重要；确定递归需要返回哪些信息也很重要（缺什么返回什么）  
特点：问题可通过某些参数来描述（e.g. root），问题必须是可分割的，且分割后规模减小但形式相同（e.g. 都是排序/找最小/...）。二叉树问题是天然的分治结构。

```python
def divideConquer(root):
    if (root is None):
        handle empty tree
        
    # only needed when it's impossible to correctly extract results from an empty tree    
    if root is a leaf node:  
        handle lead node  
        
    left results = divideConquer(root.left)
    right results = divideConquer(root.right)
    
    return combined results
```

##### BST（二分查找树）
常考数据结构 （非递归用到stack） 
思想：
> 
> - 每一个节点的左子树所有节点都小于该节点；右子树的所有节点都大于该节点
> - BST的中序遍历（in-order traversal）结果是严格递增的序列
> - BST的最小节点是一直向左走的叶子节点
> - BST查找某一个值的时间复杂度是O(h) (最差O(n), 最好O(logn))，只有balanced BST 可以达到 O(logn)

- 熟记中序遍历模板（非递归）、查找时间复杂度
- 典型题目：BinarySearchTreeIterator; how about `prev()` instead of `next()`; ClosestBinarySearchTreeValue; 

```python
# stack 中存从根到当前节点（栈顶）的路径上所有点，（optional：但剔除已被iterator/print过的节点）
# 流程：用栈存储最左的子树，从底部开始遍历，遇到有右子树的则存储右子树的最左子树，否则继续遍历
# 适用：所有二叉树的中序遍历
class BSTIterator:
    def __init__(self, root):
        self.stack = []
        self.find_most_left(root)
        
    def hasNext(self):
        return len(self.stack) > 0

    def next(self):
        curr = self.stack[-1]
        
        if curr.right:
            node = curr.right
            self.find_most_left(node)
        else:
            node = self.stack.pop()
            while self.hasNext() and (self.stack[-1].right == node):
                node = self.stack.pop()
        
        return curr
    
    def find_most_left(self, root):
        while root is not None:
            self.stack.append(root)
            root = root.left
```
 
##### 组合类DFS 
> 在非二叉树上的深度优先搜索（Depth-first Search）中，90%的问题，不是求组合（Combination）就是求排列（Permutation）。特别是组合类的深度优先搜索的问题特别的多。而排列组合类的搜索问题，本质上是一个“隐式图”的搜索问题。  
> 一个问题如果没有明确的告诉你什么是点，什么是边，但是又需要你进行搜索的话，那就是一个隐式图搜索问题了。所以对于这类问题，我们首先要分析清楚什么是点什么是边。
> - O(组合数 * 构造每个组合的时间)， e.g. O(2^n * n)

搜索过程：

- 时常需要先排序，以简化搜索和输出结果；只需要知道每个元素是否使用过，所以在每次递归中需要使用startIndex，保证不回头选前面的元素；
- 时常需要自己定义图，通常可以把某个路径/方案作为点，方案和方案之间的变换关系作为边

典型题目：subsets; subsets II; combination sum; kSum II；letter combinations of phone number I/II； wrod search I/II/III; boggle game; word ladder II

##### 排列类DFS
> 全排列问题是“排列式”深度优先搜索问题的鼻祖。很多搜索的问题都可以用类似全排列的代码来完成。包括我们前面学过的全子集问题的一种做法。在这一小节中我们需要掌握：
>
> - O(方案数 * 构造每个方案的时间), e.g. O(n! * n)
> - 排列问题的搜索树是什么样的。 
> - 普通的全排列问题怎么做
> - 有重复的全排列问题怎么做？如何在搜索类问题中去重？（排序 + 相同元素要**依次**选出）
> - 经典TSP问题的四种解法：暴力搜索，暴力搜索加剪枝，压缩状态DP，随机化算法 (genetic algorithm)

搜索过程：

- 时常需要先排序，以简化搜索和输出结果；元素前后顺序可变，所以只需用visited的哈希表保证当前元素没被使用过
- 时常需要自己定义图，通常可以把某个路径/方案作为点，方案和方案之间的变换关系作为边.

典型题目：permutations; permutations II; TSP
 
##### 组合排列类DFS - 非递归
用非递归（Non-recursion / Iteration）的方式实现全子集问题，有两种方式：

- 进制转换（binary）
- 宽度优先搜索（Breadth-first Search）
- 详细内容见 [Chapter 30](./Chapter_30_非递归排列组合DFS.md)
 
 
## 哈希表 (Hash table/map/set)
哈希表（Java 中的 HashSet / HashMap，C++ 中的 unordered_map，Python 中的 dict/ defaultdict/ set）是面试中非常常见的数据结构。它的主要考点有两个：

1. 是否会灵活的使用哈希表解决问题
2. 是否熟练掌握哈希表的基本原理

hash function 是 hash table的时间复杂度瓶颈。int, double, char 等可以达到O(1)，但string，matrix等就需要O(n) or O(n^2)的时间才能算出hash code

hashtable + linkedlist 可以实现任意位置快速查询和增删操作
hashtable + array 可以实现灵活掌控数组中某位置增删状态

重点及反复练习题：LRU Cache; insertDeleteGetRandom; FirstUniqueNumberinDataStream

## 堆 (Heap)
熟悉各种操作的时间复杂度  
熟悉heapify, siftdown, siftup操作  
uglyNumber II; topK; kClosestPoints (online vs. offline)  


## 动态规划（Dynamic programming）
Overview

- 记忆化搜索 vs 动态规划： 前者是后者的一种实现方式
- 动态规划为什么快？因为通过寻找相同问题在不同参数下的联系，避免了重复计算/ 遍历
- 动态规划 vs 分治法：分治法中问题一分为二后并没有交集，可以分别独立解决（e.g. 二叉树）；动态规划中，大问题在化解为小问题之后仍有重叠（e.g. 数字三角形），可通过记录重叠部分以避免重复搜索/ 计算
- 动态规划 vs 贪心法（greedy）：DP为了长远的利益会损失当前利益；greedy反之

适用场景：

- 求最优值
- 求方案数
- 求可行性

不适用场景：

- 求所有方案数（99%）
- 输入数据是无序的（60 -70%），有的通过先排序可以简化节目
- 暴力算法时间已经是多项式级别（80%），一般只擅长从2^n, n!优化到n^2, n^3

典型题目：wildcard matching

四要素：
state
function
initialization
answer

时间复杂度：
状态总数 * 决策数
主要从递归方程中推断






----

draft:
暂时跳过：median of Two Sorted Arrays (双指针，分治法),  Knight Shortest Path II， Word Ladder; Closest Binary Search Tree Value II; Serialize and Deserialize Binary Tree; Two Sum IV - Input is a BST; Trim a Binary Search Tree; permutations; permutations II；Triangle；bash game; Merge K Sorted Arrays; Merge Two Sorted Interval Lists; Intersection of Two Arrays



在矩阵上寻找连通块和找路径的一大区别在于，visited在连通块中是不需要回溯操作回到递归前的状态；而在路径中是需要回溯的，因为需要保证不同的路径使用不同的。

BFS中，如果需要记录每一层的distance，存distance的hashmap也可以起到visited的作用，一石二鸟

BST -> O(n)
balanced BST -> O(logn)

```postorder traversal
大致思路如下：
1、如果根节点非空，将根节点加入到栈中。
2、如果栈不空，取栈顶元素（暂时不弹出），
a.如果（左子树已访问过或者左子树为空），且（右子树已访问过或右子树为空），则弹出栈顶节点，将其值加入数组，
b.如果左子树不为空，且未访问过，则将左子节点加入栈中，并标左子树已访问过。
c.如果右子树不为空，且未访问过，则将右子节点加入栈中，并标右子树已访问过。
3、重复第二步，直到栈空。
```

When asked about counts, rather than all possible content, need a way to count in batch, rather than traverse all possibilities, or think of DP

remain un-studied: 32.6

有意思的章节video：
20.1,  20.6, 21.5，24.1，24.2, 24.3，25.1，25.2, 25.6, 25.7, 25.8，25.9, 25.10, 25.11, 26.1，26.2, 27.1
32.3, 32.4, 32.5, 32.6， 33.1, 33.2, 33.3, 33.5, 34.1, 34.2, 34.3, 35.1