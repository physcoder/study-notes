* write down the pseudo code, think about the EDGE CASES!

* Divide problem into different tasks, solve one by one

* Combine stacks, queues to store auxillary variables

ARRAYS:
* Bitwise operations: ^, ~, &, | (xor, not, and, or); a^a = 0, a^b^a = b, a^0 = a
* a^b performs adding, a&b<<1 gives the carry bits
* a>>i & 1 performs testing ith bit is 1 or 0
* python automatically converts between int, long, long long. Normally, long is 32 bits(4 bytes), is considered the length of int
* negative number is stored using two's complement. meaning that negating a number requires negating everybit and then add one to the result (+ -> - and - -> +); so for a given negative number, write out its binary form for the abs value, then negate it, thus giving it's two's complement representation
* ~0 == -1
* be careful about bit manipulation in python, negative numbers are stored as if there are infinite leading 1's, so n>>k, will eventually become -1 all the time

* String related functions: ord(), chr(), str.lstrip(), str.rstrip(), str.lower(); behaviors of split

* Get both quotient and remainder: divmod()

* Itertools has many useful functions

* try avoiding pure math of indexing, follow the patterns of the indexing change of the problems and adjust incremental steps

* when traverse a list with some criterion to group, be careful about the last group, check whether it is processed

* (a==0) != (b==0) is xor

* two pointers is a very clever method used in linked-list/array traversing/matching, when one pass is desired. move the two pointers at different times/rates according to some conditions. (ksum, merge); find the correct moving rule (containMaxWater)

* when trying to find a container or the boundaries on the two sides, it is a great idea to left scan find the largest left side, and right scan find the right largest, and finally scan again to get the final results (e.g. trap rain water) 

* rotation of an array by k steps (traversing a round table), can make use of indexing by (i+k)%L, and when get stuck just move to the next one, this way each item traversed and only traversed once; in linkedlist one can connect end to start to form a circle and then break at the desired place

* In place reverse an array, exchange the first and last one, till reach the middle.

* quickselect for finding the kth largest

* Moore's voting for finding a majority (>=n/2) in O(n)

* be careful about appending a result([]) to a list of list ([[]]), the reference to the appended list in later operations can change its value, even if it is already appended. Best way is to append a copy if any changes to the result are undesired (combinationSum.py)  

* list methods: sort, index, pop, count, reverse 


STACK:
* cases: histogram maximum; maximal rectangle; contain water; 
* can push index in, instead of actual values

LINKEDLIST:
* have a dummy head who's next points to the real head is useful sometimes in traversing a linked-list. Can take care of deleting/inseting at the head issues

* make use of circles (connect rear to front) when possible (rotatelist)

* make use of creating branches (two or more lists, then merge), like partitionlist

* two pointer method, means we can keep track of the distance k between them and also finding loops

* reverse, only need to change the direction of links between each two node, but be careful about the end

* cycle, two pointer race

* sometimes recursion can be used, clearer than iteration

* can make use of insert nodes after each node to use a hashtable (copy list)


TREE:
* serial presentation of binary tree (easily go up and down) -> heap

* breadth-first traversal of binary tree: queue; want to know each level's elements, use priority queue (level as the priority) or two queues hold this and next level

* depth-first traversal of binary tree: recursion; or stack

* a recursive solution is good, but can be really slow, if the operation is very easy and a lot of iterations are needed. Try reformulate it into a iterative one. ( fibonacci sequence )

* binary search tree is different from binary tree!

* check valid binary search tree, need to maintain the parent element.

* in order traversal of binary search tree gives sorted values


STRING:
* the Boyer–Moore string search algorithm -- efficient string searching algorithm.

* the Knuth–Morris–Pratt string searching algorithm -- build a partial matching table from the pattern indicating the backoff steps if the current position matching fails

* pattern matching, especially string, can make use of hashing, build a fast hash function that eases comparison, like A[26] storing the latest indices or number of times 

* binary search, without testing the mid==target case, can be turned into a upper_bound/lower_bound position-of-target searching algorithm (test lo==target when lo==hi)

* a hash table doesn't have to be a real set or dictionary, use the most efficient analogy for the problem, e.g. an array with its indices as the hash.

* make use of sorting string to generate keys in hashtables

* complicated pattern matching may require DP


DYNAMIC PROGRAMMING:
* dynamic programming is important. divide problem into local ones. think about all distinct local possibilities, construct a rule that can easily go from current to the next, then choose the optimal from all possible final results (maxSubarray). Depending on the situation (r[k]=f(r[k+1]), start from end; r[k]=f(r[k-1]), start from begining), choose to start from the end of the start. (starting from the end is more often). 
Define the state f[i],f[i][j],f[i][j][k]..., write out the recursion relation, then the edge cases (starting points)

* an often used criterion is the "target" ended in position i, or between i and j, or up to k ending at j, or between a[0 to i] and b[0 to j]

* sometimes does not have to follow the exact testing criterion, e.g.: 'max_len between i and j' replaced by 'is true between i and j'

* if stuck at a seemingly impossible situation, thinkg outside of the box, try to see if we really need certain requirements met.


GREEDY:
* good for solving max/min/best type of problems, in a lot of cases can solve in linear time as DP may require O(N^2), e.g. Jump Game II

* assume already have a best solution for a sub problem, focus on how to find/update the better solution based on the current one.