def quickSelect(lst, k):
    '''find the top k-th item from input list'''
    if len(lst) < k:
        return None

    l = 0
    r = len(lst)-1

    return _quickSelect(lst, l, r, k)


def _quickSelect(lst, l, r, k):

    pivot_idx = l
    pivot_val = lst[pivot_idx]

    for i in range(l+1, r+1):
        if lst[i] > pivot_val:
            pivot_idx += 1
            lst[i], lst[pivot_idx] = lst[pivot_idx], lst[i]

    lst[pivot_idx], lst[l] = lst[l], lst[pivot_idx]

    if pivot_idx == k-1:
        return lst[pivot_idx]

    elif pivot_idx > k-1:
        return _quickSelect(lst, l, pivot_idx-1, k)

    else:
        return _quickSelect(lst, pivot_idx+1, r, k)


input_lst = [1, 3, 2, 4, 5, 6, 8, 9, 7]

print(input_lst)
print(quickSelect(input_lst, 5))
print(input_lst)
