# Relational Database and SQL

Online practise/test resources:

- [HackerRank](https://www.hackerrank.com/domains/sql)
- [ModeAnalytics](https://community.modeanalytics.com/sql/tutorial/sql-business-analytics-training/)
- [AnalyticsVidhya](https://www.analyticsvidhya.com/blog/2017/01/46-questions-on-sql-to-test-a-data-science-professional-skilltest-solution/)

# Udacity
### Entity Relationship Diagram (ERD)
A common way to view data and their structures and relationships in a database
![ERD](./ERD.png)

### Types of Databases
Advantanges of using Databases: 

- ensure data integrity; 
- fast data access;
- easy data sharing;

Many different types of SQL databases:  
They offer slightly different syntax and functions when querying the data.

- MySQL
- Postgres
- Access
- Oracle
- Microsoft SQL Server


### SQL vs. NoSQL
NoSQL: Not only SQL, more popular for web based data, less for structured data. one example is **MongoDB**


### Formatting SQL code
- Capitalize all SQL keywords (e.g. `SELECT`, `FROM`)
- Put a semi-colon (`;`) at the end of an sql query/statement
- Use `.sql` for sql scripts
- Write comments. `/* My notes */`

### Querying with SQL 
#### Common SQL commands
- `ORDER BY` (ascending order by default)
    
    ```sql   
    SELECT * 
    FROM sales
    ORDER BY date DESC
    ```

- `WHERE`
    - `=, !=` works on both textual and numeric values
    - use single quotes `''` for textual data
    - logical statement: `LIKE, NOT, IN, AND, BETWEEN, OR`
        - `LIKE`: match with a pattern of text values
        - `IN`: match exactly(`=`), but with multiple items
        - `NOT`: combine with previous two: `NOT IN`, `NOT LIKE`
        - `AND & BETWEEN`: combine multiple logical statements; BETWEEN is used when multiple statements on the same column takes palce.
    
    ```sql  
    SELECT name
    FROM accounts
    WHERE name LIKE '%one%';
        
    SELECT *
    FROM web_events
    WHERE channel IN ('organic', 'adwords');
        
    SELECT name
    FROM accounts
    WHERE name NOT LIKE 'C%';
        
    SELECT occurred_at, gloss_qty 
    FROM orders
    WHERE gloss_qty BETWEEN 24 AND 29;
    ```
            
- Row-wise Calculations (derived columns):
    - arithmetic operators: `*, +, -, /`
    - set column names:
    
    ```sql
    SELECT standard_amt / total_amt AS pct 
    FROM orders
    ```
        
- `JOIN` (inner join)
    - primary key & foreign key
    - combine filtering logic with `ON` statement by using `AND` helps to speed up the query, b/c logics attached to `ON` will be executed before the join, rather than after as in `WHERE`
    - self join is often useful
    - join on inequalities (with `AND`) as additional filtering
    
    ```sql
    SELECT orders.*, accounts.*
    FROM accounts
    LEFT JOIN order
    ON accounts.id = orders.account_id
    AND accounts.sales_rep_id = 321500;
    ```

- `LEFT JOIN` and `RIGHT JOIN` (outer join)
    - return matching rows along with all other rows from the left (`LEFT JOIN`) or right (`RIGHT JOIN`) table.
    - all non-matching rows are filled with `NULL` in the columns 
    - left and right joins are interchangeable, so typically only left joins are used.
    - `OUTER JOIN` (full outer join) returns matching rows and non-matching rows from either of the two tables. The usecases are very rare

- Alias (`AS`)
    - make alias for columns or tables (`AS` is optional)
    
    ```sql
    Select t1.column1 aliasname, t2.column2 aliasname2
    FROM tablename AS t1
    JOIN tablename2 AS t2
    ```

- `NULL`
    - use `IS` and `IS NOT` for missing data, as `NULL` is not a value, but rather a property of the data
      
- Aggregation
    - `MIN, MAX, SUM, AVG, COUNT` for aggregation over rows (vertically only).
    - `COUNT(*)` v.s. `COUNT(col_name)`: count `NULL` or not
    - all other aggregate functions will ignore `NULL` entries

- `GROUP BY`
    - any column that's not within an aggregate function in `SELECT` must be also appear in `GROUP BY`
    - group by multiple columns 

- `DISTINCT`
    - return unique rows for **all columns** in the `SELECT` statement, so it's written only once in `SELECT`
    - can slow down the query, especially in aggregations

    ```sql
    SELECT DISTINCT column1, column2, column3
    FROM table1;
    ```

- `HAVING`
    - the counterpart of `WHERE` for filtering on an aggregated column
    - the key difference between `HAVING` and `WHERE` is that `HAVING` can work with aggregate functions
    - `HAVING` appears after `GROUP BY`, but before `ORDER BY`
    - such tasks can also achieved by subqueries  

- DATE functions
    - datetime in databases: `YYYY-MM-DD Hr:Min:Sec`
    - `DATE_TRUNC()`: truncate datetime to a specific level
    - `DATE_PART()`: extract specific part of a datetime
    
    ```sql
    SELECT DATE_TRUNC('day', occurred_at) AS day, SUM(total) AS total_amt
    FROM orders;
    
    SELECT DATE_PART('dow', occurred_at) AS day_of_week, SUM(total) AS total_amt
    FROM orders;
    GROUP BY 1
    ORDER BY 1
    ```
    
- `CASE`
    - SQl version of `if...else...`
    - must have at least on `WHEN... THEN...` and end with `END`, `ELSE` is optional.
    - always appear in `SELECT` clause
    - very useful in derving new columns and then perform additional operations on them.

    ```sql
    SELECT account_id,
           occurred_at,
           total,
           CASE WHEN total > 500 THEN 'Over 500'
                WHEN total > 300 AND total <= 500 THEN '301 - 500'
                WHEN total > 100 AND total <= 300 THEN '101 - 300'
                ELSE '100 or under' END AS total_bins
    FROM orders
    ```

#### Subqueries & Temp. Tables
Subqueries are queries within queries. The key difference from joins is that subqueries are used to build an output to use in a later part of the query. Often times, a problem can be solved by either subqueries or joins, but tradeoffs in speed and performance need to be considered to take the most appropriate approach. 

Typical usecases:

- Aggregation to a different level (e.g. daily to monthly)
- Return a scalar to be used later

Basics:

- must be fully placed inside parentheses
- mostly independent and can be executed alone
- placements and dependencies
- [more details](https://docs.microsoft.com/en-us/sql/relational-databases/performance/subqueries?view=sql-server-ver15)
![](./subquery_basics.png)

Placements
![](./subquery_1.png)

```sql
/****** WITH ******/
WITH average_price AS
( 
    SELECT brand_id, AVG(product_price) AS brand_avg_price
    From product_records
),
SELECT ... /* some other queries */

/****** Nested ******/
SELECT *
FROM students
WHERE student_id
IN ( SELECT DISTINCT student_id
     FROM gpa_table
     WHERE gpa >3.5
   );
``` 

##### Dependencies

- Typically better to write simple subqueries that can be executed separately
- Correlated subqueries depend on values from the outer query. It's useful when there are filterings that should change according to values from the outer query. e.g.:
  
  ```sql
  SELECT first_name, last_name, (
                 SELECT AVG(GPA)
                 FROM outer_db
                 WHERE university = outer_db.university) GPA, university
  FROM student_db outer_db;
  ```

##### Views

Views are virtual tables derived from base tables that can be re-queried later. Views are not persistent stored in the database, they live in the memory.
View definitions can be altered, but updating the data in a view may or may not update the underlying base table.

```sql
CREATE VIEW <VIEW_NAME>
AS
SELECT …
FROM …
```

##### `WITH` for reusable temporary tables
Multiple reusable tables can be created with one `WITH` statement, tables are separated by `,`, except for the last one.

- more readable 
- more efficient

```sql
WITH table1 AS (
          SELECT *
          FROM web_events),

     table2 AS (
          SELECT *
          FROM accounts)

SELECT * 
FROM table1
```

##### subquery tradeoffs

- readability: How easy it is to determine what the code is doing
- query plan: What happens under the hood
- performance: How quickly the code runs
- [optimizing SQL performance](https://dev.mysql.com/doc/refman/8.0/en/optimization.html)

![](subquery_strategy.png)

#### Data Cleaning with SQL 
A handful of useful functions:

- `Left()`:   Extracts a number of characters from a string starting from the left
- `Right()`:  Extracts a number of characters from a string starting from the right
- `Substr()`: Extracts a substring from a string (starting at any position)
- `Position()`: Returns the position of the first occurrence of a substring in a string
- `Strpos()`: Returns the position of a substring within a string
- `Concat()`: Adds two or more expressions together
- `Cast()`:   Converts a value of any type into a specific, different data type
- `Coalesce()`: Returns the first non-null value in a list 

Others include ([more details](https://mode.com/sql-tutorial/sql-string-functions-for-cleaning/)):

```sql
UPPER(), STRING_SPLIT(), ROW_NUMBER(), LENGTH(), REPLACE(), ISNULL(), etc.
```

#### Window functions
A window function is a calculation across a set of rows in a table that are somehow related to the current row. This means we’re typically:

- Calculating running totals that incorporate the current row or,
- Ranking records across rows, inclusive of the current one

A window function is similar to aggregate functions combined with group by clauses but have one key difference: Window functions retain the total number of rows between the input table and the output table (or result). Behind the scenes, the window function is able to access more than just the current row of the query result. A window is defined by the 'Order' and the 'Partition'.
![](window_function.png)

```sql
AGGREGATE_FUNCTION (column_1) OVER
 (PARTITION BY column_2 ORDER BY column_3)
  AS new_column_name;

/* running total, partition by is optional */
select standard_amt_usd,
       sum(standard_amt_usd) over (order by occurred_at)
       as running_total
from orders
```

Explaining why a running total/average is possible: 

- Without a `PARITION BY`, the window size will be the whole column. Aggregations will be performed from the top, expanding till it hits the bottom (the whole partition), then sliding to the next position, which of course there isn't any at that point. 
- Without a `ORDER BY`, the order within each window can not be relied on.

##### Aliases
reuse the same window definitions (placed at the bottowm of the query):

```sql
WINDOW window_name AS (PARTITION BY column_1 ORDER BY column_2)
```

##### Lead & Lag
return values from the prior row (`LAG`) or following row (`LEAD`), so that comparisons can be made to the current row

```sql
LAG(standard_sum) OVER (ORDER BY standard_sum) AS lag
LEAD(standard_sum) OVER (ORDER BY standard_sum) AS lead

```

#### `UNION` - append data
two structurally similar tables (idealy same schema) can be appended together to form a combined table, with duplicate rows dropped, by `UNION`

- the two queries to be unioned must have the same number of fields in `SELECT` and the fields must be of similar data types  
- `UNION` vs `UNION ALL`: retain the duplicated rows or not

```sql
SELECT * 
FROM web_events

UNION 

SELECT *
FROM web_events_2
```

### Performance Tuning
- Table size: use `LIMIT` for exploratory analysis on subset data
- Aggregation: aggregation can be slow and is performed before the final `LIMIT`; use `LIMIT` early in subqueries to reduce rows
- Join: filter/group-by before join; add filters to `ON...AND...`
- Database framework: perform operations that are optimized for this particular DB (Redshift, Postgres, etc.)


# Previous Notes

- database == a set of named relations (tables)
- **relation** == a set of named attributes (columns)
- each **attribute** has a type (domain)
- each **row** (tuple) has a value for each attribute
- **schema** == structural description of relations in database (table names, attributes and their names)
- A relation schema is the logical definition of a table - it defines what the name of the table is, and what the name and type of each column is. It's like a plan or a blueprint. A database schema is the collection of relation schemas for a whole database. so schema:database:table is analogous to floorplan:house:room.
- **instance** == the actual content of the schema at a given time point, which changes over time
- **NULL** == a special type that declares 'unknown' value in any attributes
- **key** == a special attribute or set of attributes that give(s) unique value for each row. A lot of databases build index structures based on the keys, so it is very fast to query by the key. Keys are usually written with underscores in documentations.


## Relational Algebra
A relation is produced after any of the operations, so other operations can be done on the results

- Select operator: $\sigma$
    - select rows: $\sigma_{condition1 ^ condition2} RelationName$

- Project operator: $\Pi$
    - select cols: $\Pi_{(attr1,attr2,...)} RelationName$

- Union operator: U
    - combine two relations together, must have the same schema, eliminates duplicate values

- Difference operator: -
    - subtract the latter list from the former one, must have the same schema

- Intersect operator: ∩
    - only retain rows that have the same values for the common attributes. The order matters.
    - equivalent to the Natural Join operator ⋈

## XML Data
Extensible Markup Language, a data model alternative to the relational model.
- tags represents contents, rather than format (unlike HTML)
- structure: nested hierarchy; tagged elements; attributes; texts
- example: <tag attr1='',attr2=''> text </tag>
- Document Type Descriptor(**DTD**) and XML Schema (**XSD**) are separate languages(files) that specifies the structure (attributes, elements, nesting, ordering, pointers/keys) of the XML file
- **DTD** is a grammatical language that resembles the regular expression.
- **XSD** is written in XML itself, more powerful and more extensive.

## JSON Data
- a textual format consists of entries formatted in a data dictionary form ["key1":value1, "key2":value2] or {"key1":value1, "key2":value2}
- the dictionary can be nested.

## SQL
Pronounce: "sequel"

- `select` ATTR1, ATTR2 `from` TABLE `where` CONDITION
- `like` for string matching
- `as` for renaming
- `table.attr` for differentiating duplicate variable names
- `distinct` for deduping
- `order by` for ordering the table
- Set operators: `union`, `intersect`, `except`
    - `union` may return the results dedupped (definition of SET) and sorted depending on system, `union all` returns the raw multiset results.
    - `intersect` may not be supported on every RDBMS system, but can be worked around by other names
    - `except` is the difference (minus operator)
- `<>` for not equal
- `in` and `not in` for testing in a set or not
- `exists` for testing empty or not
- `all` and `any` for specifying relationships to a whole group of results; can be rewritten using `exists` and `not exists`
- `min, max, avg, count` for aggregation over rows. `count(*)` return number of rows; `count(distinct column)` returns number of unique rows.
- `group by COLUMNS having CONDITIONS` for partitioning and filtering on a group level (conditions in `where` applies to a single row)
- `NULL` is used to represent any unknown values. However, such entries will be ignored in logical comparisons or `count()`, but included in `distinct`. Testing `NULL` can be written as `is NULL` and `is not NULL`. Be careful about potential NULL values in the SQL queries.
- subqueries is powerful
- **modify** the database
    - `insert into TABLE_NAME values (ATTR1, ATTR2, ...)`
    - `delete from TABLE_NAME where CONDITIONS`
    - `update TABLE_NAME set ATTR1=val1, ATTR2=val2, where CONDITIONS`
- **join** operators
    - PostgreSQL requires join operators being binary
    - in theory, SQL should find the best way of executing equivalent queries; but in reality, similar queries perform very differently.
    - **inner join**: `TABLE1 inner join TABLE2 on CONDITION`, the default of **join**
    - **natural join**: `TABLE1 natural join TABLE2`, special case of **inner join**, automatically selects rows that has the same values in all common column names and concatenate all columns from both tables, eliminate duplicate column names
    - **join using**: `TABLE1 join TABLE2 using(ATTR1,ATTR2)`, inner join on specific columns, columns have to be a subset of the common columns, better software engineering practice
    - `using` and `on` can not appear in a query at the same time, use `where` if needed.
    - **outer join**
        - **left outer join**: preserves all entries in the first table, when there is no matching values in the columns of the second table, still add the columns but pad with `NULL`, same as **left join**
        - **right outer join**: the opposite of the above preserving all entries in the second table.
        - **using**: also valid in outer joins
        - **full outer join**: retains all entries in both tables, if there is no match in either of the tables, pad `NULL` in those columns, equivalent to the union of left outer join and right outer join
        - outer joins are not associative, so the order of outer joins are important in determining the output results
        - full outer joins are commutative

- **case when** can be used to fetch based on different conditions.
    - compares equality of `EXP` and each condition `EXPRn`  
    ```case EXPR
       when EXPR1 then RES1
       when EXPR2 then RES2
       else RESn
       end
    ```
    - executes each boolean to determine conditions
    ``` case
        when BOOL_EXP1 then RES1
        when BOOL_EXP2 then RES2
        else RESn
        end
    ```
    - sequential execution, so later clauses are on the data left behind (implicit `else`); good for counting things, transforming and creating new variables

- *primary key* is the unique identifier in a table that is usually indexed for faster searching. *logical key* is the column used for the 'outside world' to reference a row in the table in a more human-readable manner. *foreign key(s)* are the columns used to connect to other tables.

- **EXPLAIN** shows the time cost of the query, look at cost and its components (cost is relative)

- Subqueries are good for one to one relationship, usually limit to 1 to upon returning so that the outer query only compares to one row

- **using** vs. **on**. **using** is used for joining on common column names.  

- When joining tables, it's better to start with the most general table, go into more details at each join (e.g. loans -> tradelines -> account_history), the latter tables would have more granular information meaning there will be multiple rows corresponding to one id in the most general table. draw out the relationships will be very helpful

- Indexing. It increases performance and all index columns are equivalent (no main or secondary). Indexes are stored separately, usually unique id and foreign keys are indexed, but others can also be indexed based on design (such as being accessed frequently in **where** clauses)

- If no perfect one to one relationship in **join**, then usually use **order by** or **group by** and limit 1 to enforce returning only one row (in other words, returning the largest/latest)

- Aggregate functions are usually used with **group by**


    group by may be faster than distinct


    create your own table ,and then from that do aggregate,  conceptually easier, first only worry about getting the right rows. alternative for case when

    limit 1 always returns the first row

    distributed system: disadvantages

        w3school

- One-to-Many relationship is the most intuitive model between tables, just like loans vs. transactions. One-to-Many is modeled by setting a foreign key in the 'Many' table that matches the primary key in the 'One' table.
- Many-to-Many relationship is also frequently encountered, like courses vs. students; books vs. authors. Many-to-Many relationship is modeled by having a junction table in between that doesn't not have a unique primary key, but rather a composite primary key pairing the primary keys from the two 'Many' tables so that one 'Many-to-Many' is mapped into two 'One-to-Many' relationships. In the case of courses vs. students, a third table 'membership' can be set up with primary key (course_id, student_id), maybe additional attributes such as 'role' (student, TA, grader) storing additional information about this relationship
__