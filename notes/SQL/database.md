# Data Management and SQL

## SQL vs. NoSQL

#### SQL: Relational Database

- Based on relational models (RDBMS)
- e.g. Postgres, MySQL, SQLite

#### NoSQL: Non-relational Database

Non-relational databases started to be developed as a result of the growing data needs of the modern web, both in terms of volume of data and its bandwidth. These needs required being able to scale database servers "horizontally" by adding more machines and networking them together, something that relational databases weren't initially designed for

- There is great value in combining different databases for different needs
- Usecases:
    - Key-value stores
    - Graph databases
    - Document stores

![](./nosql_types.png)

- e.g. MongoDB, Redis, ElasticSearch

| MongoDB | Redis |
|---------|-------|
| document-oriented, NoSQL              | fast in-memory key-value store |
| provides some transactional guarantees| all data has to fit into memory|
| schemaless, "just insert data"        | suppports many datastructures  |
| can scale horizontally                | runs on single machine         |


## Design Schemas: Data Normalization 
### First Normal Form (1NF)
- Make all values in a column consistently of the **same data type**
- Make sure each cell contains only a **single value**
- Make sure there are **no repeating columns** for the same data (e.g. category 1, 2, 3, …)
- Enable the rows of data to be **uniquely identified** through a column or combination of columns (sometimes surrogate keys are needed)

### Second Normal Form (2NF)
- Satisfy 1NF
- **Remove partial dependencies** by creating additional tables. Partial dependencies happens when non-key columns depend on part of the key/id columns.

### Third Normal Form (3NF)
- Satisfy 2NF
- **No transitive dependencies** (one non-key column determines another non-key column)

### A Pragmatic Approach
- Step 1: identify entities 
- Step 2: create a table for each one (with abstract ID column instead of human entered data)
- Step 3: go back to the original table and relate IDs to each other
- Step 4: review and verify all three norms
- Over-normalization could lead to complex `join`s, the 3NF may be violated at times.

## Create Schemas: Data Definition Language (DDL)
These commands are used to create, update, or delete table schemas directly, different from manipulating data in the table.

```sql
/** Create Table Schema **/
CREATE TABLE "table_name" (
"column_name" column_type,
"id" SERIAL,
"product name" TEXT,
"quantatity" INTEGER
)
```

```sql
/** Update Table Schema **/
ALTER TABLE "table_name" some_action;
ALTER TABLE "users" ADD COLUMN "email" VARCHAR;
ALTER TABLE "users" ALTER COLUMN "last_name" SET DATA TYPE VARCHAR;
ALTER TABLE "users" DROP COLUMN "nickname" VARCHAR;
```

```sql
/** Delete/Describe Table Schema/Data **/
DROP TABLE "table_name";
TRUNCATE TABLE "table_name";
TRUNCATE TABLE "table_name" RESTART IDENTITY;
COMMENT ON COLUMN "table_name"."columna_name" IS "some descriptions"
```

### Data Types
#### Numeric data 
- integers: 
    - normal integer: **`INTEGER`**, `SMALLINT`, `BIGINT`
    - serial: **`SERIAL`**, `SMALLSERIAL`, `BIGSERIAL`
    - if we don't give it a value when inserting data, Postgres will automatically generate the next integer in sequence, until the sequence is exhausted based on the range
- decimals:
    - floating point (inexact): `REAL`, `DOUBLE PRECISION`  
    - numeric (exact): `NUMERIC` or `DECIMAL`
    - exact decimal data is much slower to compute than inexact data, but is free of rounding errors (e.g. prices, interest rates, timestamps)

#### Text data
- no limit: **`TEXT`**
- variable length: **`VARCHAR(n)`**
- fixed length: `CHAR(n)`
- `CHAR` is used rarely, `TEXT` is usually to store large amount of text.

#### Date/Time data
- date and time: **`TIMESTAMP`**, `TIMESTAMP WITH TIME ZONE`
- date only: **`DATE`**
- time only: **`TIME`**
- storing time zone or not is very important for different usecases
- `CURRENT_DATE` and `CURRENT_TIMESTAMP` are provided by SQL for datetime comparisons

## Update Data: Data Manipulation Language (DML)
`INSERT` data into tables

```sql
-- template:
INSERT INTO "table_name" (column list...) VALUES (row of values), ...
INSERT INTO "table_name" (column list...) SELECT ... FROM ...

-- examples:
INSERT INTO "movies" ("name", "release_date") VALUES
 ('EP1 - A New Hope', '1977-05-25'),
 ('EP4 - The Empire', '1980-05-01'),
 ('EP3 - Return', '1983-05-24');

INSERT INTO "movies" ("id", "name", "release_date") VALUES
 (DEFAULT, 'EP1 - A New Hope', '1977-05-25'),
 (DEFAULT, 'EP4 - The Empire', '1980-05-01'),
 (DEFAULT, 'EP3 - Return', '1983-05-24');

INSERT INTO "movies" ("id", "name", "release_date") SELECT "id", "name", "release_date" FROM "movie_reviews" WHERE "id" > 500
```

`UPDATE` data already in tables

```sql
-- template:
UPDATE "table_name" SET "col1_name"=new_val, ... WHERE ...

-- example:
UPDATE "users" SET "mood" = 'Low' WHERE "happiness_level" < 33;

UPDATE "people" SET "date_of_birth" = 
  (CURRENT_TIMESTAMP - "born_ago"::INTERVAL)::DATE; -- born_ago has the format: "34 years 1 month 2 day"
  
UPDATE "posts" SET "category_id" = (
  SELECT "id"
  FROM "categories"
  WHERE "categories"."name" = "posts"."category"
);

```

`DELETE` data from tables (rows only)

```sql
-- template:
DELETE FROM "table_name" WHERE ...

-- example:
DELETE FROM "users" WHERE (CURRENT_TIMESTAMP - "date_of_birth") < INTERVAL "21 years";
```

#### Quick Summary
- Removing a table from the system: **`DROP`**
- Removing all data from a table:   **`TRUNCATE`** 
- Removing some data from a table:  **`DELETE`**
- Removing a column from a table:   **`ALTER`**


## Data Manipulation: Transactions
One-off commands are typically used by end-users to query data. However, most real-life situations require multiple manipulations to be orchestrated in order to achieve the desired result, herein comes the concept of transactions. Relational databases provide transactional guarantees, **ACID**:

- **Atomicity**: The database guarantees that a transaction will either register all the commands in a transaction, or none of them.
- **Consistency**: The database guarantees that a successful transaction will leave the data in a consistent state, one that obeys all the rules that you've setup. We've seen simple rules like limiting the number of characters in a VARCHAR column, and we'll see many more in the next lesson
- **Isolation**: The database guarantees that concurrent transactions don't "see each other" until they are committed. Committing a transaction is a command that tells the database to execute all the commands we passed to it since we started that transaction.
- **Durability**: The database guarantees that once it accepts a transaction and returns a success, the changes introduced by the transaction will be permanently stored on disk, even if the database crashes right after the success response.

Databases are typically accessed by multiple people simultaneously and through application code. A programmatic way of isolating each trasaction is needed:

- `BEGIN` / `START TRANSACTION`
- `COMMIT` / `END`
- `ROLLBACK`
- Any code contained between `BEGIN` and `END` will be isolated to other transactions, and can only be successfully committed when all commands go through; only committed changes will be reflected in other queries
- Also a good way to explore an unfamiliar table without risking destructions

```sql
-- template:
BEGIN;
sql code
COMMIT;

-- example:
BEGIN; -- Do everything in a transaction

ALTER TABLE "user_data" -- Split the name column in first_name and last_name
  ADD COLUMN "first_name" VARCHAR,
  ADD COLUMN "last_name" VARCHAR;

UPDATE "user_data" SET
  "first_name" = SPLIT_PART("name", ' ', 1),
  "last_name" = SPLIT_PART("name", ' ', 2);

ALTER TABLE "user_data" DROP COLUMN "name";

END; -- End of transaction
```

## Data Constraints
Constraints can be added when a table is created (`CREATE TABLE`) or after a table is created (`ALTER TABLE ... ADD CONSTRAINT ...`), to ensure certain columns meet certain requirements, such having unique values.

- Unique Constraints:

```sql
CREATE TABLE "users" (
"id" SERIAL,
"username" VACHAR UNIQUE
);

CREATE TABLE "users" (
"id" SERIAL,
"username" VACHAR,
UNIQUE("username")
);

CREATE TABLE "leaderboards" (
"game_id" INTEGER,
"player_id" INTEGER,
"rank" SMALLINT,
UNIQUE("game_id", "rank")
);

ALTER TABLE "users" ADD CONSTRAINT "unique_username_constraint" UNIQUE("username");

ALTER TABLE "users" ADD UNIQUE("username");
```

- Primary Key Constraints: 
    - UNIQUE + NOT NULL
    - only one per table

```sql
CREATE TABLE "users" (
"id" SERIAL, PRIMARY KEY
"username" VACHAR UNIQUE NOT NULL
);

CREATE TABLE "users" (
"id" SERIAL, 
"username" VACHAR,
PRIMARY KEY ("id"),
UNIQUE ("username")
);

CREATE TABLE "users" (
"id" SERIAL, 
"username" VACHAR,
CONSTRAINT 'user_pk' PRIMARY KEY ("id"),
CONSTRAINT 'unique_usernames' UNIQUE ("username")
);
```

- Foreign Key Constraints:
    - restrict the values in a column to only values that appear in another column
    - by default, `REFERENCES` uses the primary key of the table referenced.
    - adding foreign key constraints makes deleting related entities later more systematic and automatic.

```sql
/*--
CREATE TABLE "users" (
  "id" SERIAL PRIMARY KEY,
  "username" VARCHAR UNIQUE
);
--*/

CREATE TABLE "comments" (
  "id" SERIAL PRIMARY KEY,
  "user_id" INTEGER REFERENCES "users",
  "content" TEXT
);

CREATE TABLE "comments" (
  "id" SERIAL PRIMARY KEY,
  "user_id" INTEGER REFERENCES "users" ("id"),
  "content" TEXT
);

-- the CASCADE modifier can handle deleting all relevant data associated (comments related to a deleted user)  
CREATE TABLE "comments" (
  "id" SERIAL PRIMARY KEY,
  "user_id" INTEGER REFERENCES "users" ("id") ON DELETE CASCADE,
  "content" TEXT

-- the SET NULL modifier can handle setting all associated references to NULL (retain comments, but the user id is now updated to NULL)    
CREATE TABLE "comments" (
  "id" SERIAL PRIMARY KEY,
  "user_id" INTEGER REFERENCES "users" ("id") ON DELETE SET NULL,
  "content" TEXT
```

- Check Constraints:
    - customized constraints are more flexible and can be used to create various business requirements at the database level. They can be added either after a table was created, or during table creation

```sql
-- template
CHECK (some expression that returns true or false)

-- example

-- Usernames need to have a minimum of 5 characters
ALTER TABLE "users" ADD CHECK (LENGTH("username") >= 5);

-- A book's name must start with a capital letter
ALTER TABLE "books" ADD CHECK (
  SUBSTR("name", 1, 1) = UPPER(SUBSTR("name", 1, 1))
);
```
## Indexes

- one can create indexes not only on columns, but also on expressions
- one table can have multiple indexes (even on the same column) to facilitate different kinds of common searches/queries
- one index can be multi-column, for multi-column search, the order of the columns matters.
- Postgres will create a unique index for efficiency when a unique constraint is added; unique constraints can be created by `CREATE UNIQUE INDEX ON`
- adding a primary key constraint will automatically create a unique index on the column

```sql
-- template
CREATE INDEX ON table_name (column_name);

CREATE INDEX ON table_name (SOME_FUNCTION(column_name) = value);

CREATE INDEX ON "table_name" ("column1", "column2");

DROP INDEX index_name

-- examples

-- We need to be able to quickly search for books by their titles
CREATE INDEX "find_books_by_partial_title" ON "books" (
  LOWER("title") VARCHAR_PATTERN_OPS
);

-- We need to be able to quickly tell which books an author has written.
CREATE INDEX "find_books_by_author" ON "books" ("author_id");
```
#### `EXPLAIN` a (slow) query
SQL can explain the cost of a query before executing the query, using statistics of the target table SQL already keeps about (stored separately)

`EXPLAIN` outputs the query plan, which is represented in the form of a tree, where the innermost nodes are the ones that get executed first, having their output piped to the input of nodes higher in the hierarchy.

- "Seq. Scan", a sequential scan on a column, which could be slow on large datasets, but could be preferred by SQL on small datasets.
- "Bitmap Index Scan", a variant of index scan which will return a substantial amount of data, instead of a few matched rows.
- "Index Scan", a scan on an indexed column, typically fast and returns only one or few matched rows
- Learning to understand the output of `EXPLAIN` is very useful.

`EXPLAIN` returns the estimated cost/time, without executing the query; `EXPLAIN ANALYZE` returns the actual cost/time by running the query

```sql
EXPLAIN some_query

EXPLAIN ANALYZE some_query
```
#### Trade-offs (adding too many indexes)
- extra disk space to store the indexes
- more I/O processing when updating the table (`UPDATE/ DELETE/I NSERT`)
- Indexes are not the only solution to make queries run faster
    - frequency of a query
    - fulltext index
    - separate copy of a table
    - NoSQL for text and non-relational data

#### Case study:
Design a Movie Review Database
![](./movei_database.png)

```sql
CREATE TABLE "movies" (
  "id" SERIAL PRIMARY KEY,
  "title" VARCHAR(500),
  "description" TEXT
);


CREATE TABLE "categories" (
  "id" SERIAL PRIMARY KEY,
  "name" VARCHAR(50) UNIQUE
);

CREATE TABLE "movie_categories" (
  "movie_id" INTEGER REFERENCES "movies",
  "category_id" INTEGER REFERENCES "categories",
  PRIMARY KEY ("movie_id", "category_id")
);

CREATE TABLE "users" (
  "id" SERIAL PRIMARY KEY,
  "username" VARCHAR(100),
);
CREATE UNIQUE INDEX ON "users" (LOWER("username"));

CREATE TABLE "user_movie_ratings" (
  "user_id" INTEGER REFERENCES "users",
  "movie_id" INTEGER REFERENCES "movies",
  "rating" SMALLINT CHECK ("rating" BETWEEN 0 AND 100),
  PRIMARY KEY ("user_id", "movie_id")
);
CREATE INDEX ON "user_movie_ratings" ("movie_id");

CREATE TABLE "user_category_likes" (
  "user_id" INTEGER REFERENCES "users",
  "category_id" INTEGER REFERENCES "categories",
  PRIMARY KEY ("user_id", "category_id")
);
CREATE INDEX ON "user_category_likes" ("category_id");
```

## NoSQL databases
### MongoDB
- start by adding new *documents* to *collections*
- *documents* are stored in `BSON` format (binary json)
- every *document* in Mongo has an **`_id`** field which uniquely identifies it in its *collection*
- built-in functions such as `find()`, `insertMany()`, `countDocuments()` are available for all interface drivers.

| Relational | MongoDB |
|------------|---------|
| table      | collections  |
| row        | document     |
| column     | field        |
| join       | embed/reference       |
| SQL        | drivers from js, python, c++, etc. |

![](./mongoDB_example.png)
#### Design Pattern
Keypoints:

- Forget relational, think documents
- Disk space is cheap, meaning (some) duplication is ok
- Data should be stored in the way it will be accessed.
- 16MB document limit

**Common Patterns**

- Polymorphism

Having one system storing data of different forms. 

If the data items to be stored have marked difference whilst sharing some common information, a relational database would create the union of all fields and insert `NULL` for unavailable columns. However a MongoDB system will store the data as is, which would more storage efficient and logically sensible.

- Extended Reference

Duplicating certain information in the reference to another collections through an embedded document.

This is related to "*storing the data in the way it will be accessed*". One example would be to store author names (redundantly) in a books collection in reference to the authors collection, so that some text (author name) can be displayed in the webpage for visitors to click on to get to the more detailed author information.

- Subset

Storing only a subset (latest, smallest, etc.) of a list of items, where the length of the list can be very long.

This is also related to "*storing the data in the way it will be accessed*", allowing more flexibility and saving `JOIN` compuations

- Outlier

When a small minority of documents in a collection -- the outliers -- would prevent the collection from working in its currently designed schema, spearate indicators can be added for those data entries and the additional data can be separately stored and accessed.

### Redis
An in-memory key-value store which supports many basic and useful data structures. Redis can be used as a standalone database, or to add new powers to an application that relies mainly on a relational database such as Postgres.

- Strings: `GET`, `SET`
- Integers: `GET`, `SET`, `INCR`
- Lists: `LPUSH`, `LPOP`, `RPUSH`, `RPOP`, `LLEN`, `LRANGE`, `LINDEX`
- Sets: `SADD`, `SMEMBERS`, `SPOP`, `SCARD`
- Hash: `HSET`, `HMSET`, `HGET`, `HGETALL`, `HDEL`
- Explore a Redis data store: `KEYS *`
- Commands can be grouped and executed together to ensure atomicity: `MULTI`, `EXEC`
- Important to use appropriate data structures for different applications