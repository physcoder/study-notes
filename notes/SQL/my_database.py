'''
A minimal sql database interface for SQL exercises

Tutorial: https://leportella.com/english/2019/01/10/sqlalchemy-basics-tutorial.html
'''

import sqlalchemy as db

class MyDB(object):
    def __init__(self, database_path=None):
        '''
        if database_path is given, it must start with '/'
        '''
        if database_path is None:
            database_path = 'my_database.db'

        self.engine = db.create_engine("sqlite:///" + database_path)
        self.engine.echo = True
        self.metadata = db.MetaData(self.engine)


    def create_table(self, table_name, **schema):
        '''
        schema = {column_name: data_type}
        data_type can be Integer, String(80)
        '''
        if table_name in self.engine.table_names():
            raise ValueError(f'Table {table_name} already exists!')

        with self.engine.connect() as conn:

            column_definitions = [db.Column(col, dtype) for (col, dtype) in schema.items()]
            table = db.Table(table_name, self.metadata, *column_definitions)

            table.create()


    def insert_data(self, table_name, *data):
        '''
        data = [{col1: row1_val1, 'col2': row1:val2},
                {col1: row2_val1, 'col2': row2:val2}]
        Any columns not specified will be filled with NULL, except for the primary key
        '''
        table = db.Table(table_name, self.metadata, autoload=True, autoload_with=self.engine)

        with self.engine.connect() as conn:
            i = table.insert()
            i.execute(data)


    def query(self, sql):
        '''
        execute raw sql query and print results
        '''
        with self.engine.connect() as conn:
            results = [row for row in conn.execute(sql)]

        for row in results:
            print('\t'.join(str(col) for col in row))

        return results



if __name__ == "__main__":

    mydb = MyDB()
    users_schema = {'id': db.Integer, 'name': db.String(40), 'new_customer': db.Boolean}
    products_schema = {'product_id': db.Integer, 'product_name': db.String(40), 'in_stock': db.Boolean}

    mydb.create_table('users', **users_schema)
    mydb.create_table('products', **products_schema)

    mydb.insert_data('users',
                     *[{'id': 1, 'name': 'mike', 'new_customer': True},
                     {'id': 2, 'name': 'kate', 'new_customer': True},
                     {'id': 3, 'name': 'dave', 'new_customer': False}])

    mydb.query('select * from users where name=="mike"')
