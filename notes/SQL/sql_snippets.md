# SQL snippets

## the Tables
The unique IDs in each table are *emphasized*

| Table | Schema |
|:-----:|:------:|
|College|(*cName*, state, enrollment)|
|Student|(*sID*, sName, GPA, sizeHS)|
|Apply  |(*sID*, *cName*, *major*, decision)|


## Finding the max
- Option 1:
```
select cName
from College C1
where not exists (select * from College C2
                  where C2.enrollment>C1.enrollment)
```
- Option 2:
```
select sName, GPA
from Student
where GPA >= all (select GPA from Student)
```

## Outer join without `outer join`
```
select sName, Student.sID, cName, major
from Student, Apply
where Student.sID=Apply.sID
union
select sName, sID, NULL, NULL
where sID not in (select sID from Apply)
```

## Some Queries
- 1

```sql
select *
from College
where cName not in (select distinct cName
                    from Apply
                    where major='CS')
```

- 2

```sql
select sID
from Student
where GPA<3.6 and (sID in (select sID
                            from Apply
                            where 'Carnegie Mellon'=cName))
```

- 3
 
```sql
select sID
from Student S1
where GPA > all ((select GPA from Student S2 where S2.sID<>S2.sID) and
                 (sID in select distinct sID from Apply where major='EE'))
```

## Find the Nth largest
sort asc and limit N, sort desc and return first row by limit 1

## Moving average
```sql
SELECT MarketDate,
       ClosingPrice,
       AVG(ClosingPrice) OVER (ORDER BY MarketDate ASC ROWS 9 PRECEDING) AS MA10
FROM   @DailyQuote
```

## Window function
```sql
SELECT start_terminal,
       duration_seconds,
       SUM(duration_seconds) OVER
         (PARTITION BY start_terminal ORDER BY start_time)
         AS running_total
FROM tutorial.dc_bikeshare_q1_2012
WHERE start_time < '2012-01-08'
```

## Reuse multiple queries
```sql
WITH temp_table_1 AS (
     SELECT ...
     )
   , temp_table_2 AS (
     SELECT ...
     )
SELECT  
```