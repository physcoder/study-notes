# Effective Python

## Coding Style
- Use `if not VAR` instead of `if len(VAR)` to check emptiness
- Always use absolute names for modules when importing them, not names relative to the current module’s own path
    - e.g. `from Module import Submodule`
- Characters can be represented by either `str` or `bytes`, which are unicode string and raw binary formats. Always deal with unicode strings in the program and do the conversion to bytes at the furthest boundary.(`.encode()` and `.decode()`)
- Move complex expressions into helper functions, don't get obsessed with single-line complicated operations
- `dictionary` and `set` has their own list comprehensions,
    - `{v:k for k,v in dict.items()}`
    - `{v for v in dict.values()}`
- Use generator expressions when the input is large in size, so that the program does not need to hold all content in memory.
    - changing `[]` to `()` in list comprehension results in a generator
    - `(len(line) for line in open(file))`
    - generators can be chained, which runs very fast. `a = (bb for bb in b); (aa*2 for aa in a)`
- Prefer `enumerate` over `range`
    - `enumerate(list, 1)` starts counting from 1.
- Understand and make use of `try/ except/ else/ finally`;
    - `finally` is often used for guaranteed clean-up procedures, like closing files.
    - `else` minimizes the code in `try` block, so only certain exceptions can be caught. Also it is used to perform additional tasks after a successful `try`

## Functions
- Raise exceptions rather than return `None` for special situations; expect the calling code to handle properly




# Beyond the Basics
- use *generator*, which is produced by a *generator function* (a function that use `yield` instead of `return`), it is also called a *generator object*, as returned by the *generator function*.
    - *iterator* vs. *generator*, both works with built-in `next` function, which also supports default value, or it will raise `StopIteration`;
    - *iterator* is more general, which produces a value in a sequence one at at a time, it defines `__iter__` and `__next__`;
    - an object is *iterable* if it supports a `__iter__` method;
    - a *dictionary view* is superior than *iterator*, almost in every usecase. `items()` in python 3 is now the old `viewitems()`;
    - *generators* have states, the function resumes from the last `yield` statement, rather than the first line of the function;

- *list comprehension*, python interprets from left to right, so the order of `for` clause matters, otherwise a `ValueError` would be raised. The comprehension should be **split** and **indented** across multiple lines. There are also *dictionary comprehension*, *set comprehension with {}*, and *generator comprehension with ()* (no need to define a generator function)
- `import collections` module, useful collections include `namedtuple` like a `C struct`, `defaultdict`, `OrderedDict`
