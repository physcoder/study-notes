class A(object):
    def __init__(self):
        print('init A init!')

    def go(self):
        print('go A go!')

    def stop(self):
        print('stop A stop!')

    def pause(self):
        print('pause A pause!')

# `super` calls base class method
class B(A):
    def __init__(self):
        pass

    def go(self):
        super(B,self).go()
        print('go B go!')

    def stop(self):
        super(B, self).stop()
        print('stop B stop!')

    def run(self):
        print('run B run!')

# no __init__ defined, will call base class's __init__
class C(A):
    def go(self):
        super(C, self).go()
        print("go C go!")

    def stop(self):
        super(C, self).stop()
        print("stop C stop!")

    def run(self):
        print('run C run!')

# the order of construction is right to left (B first, then C, then D, in calling __init__); that is constructing from the base class
# the order of search for `run` is left to right; that is methods are searched from the most derived class
# super is not a must
class D(C,B):
    def go(self):
        super(D, self).go()

    def stop(self):
        print("stop D stop!")

    def pause(self):
        print('pause D pause!')
