# Java Application with `Maven` & `Spring Boot`

### Java Overview

Benefits:

- A good balance between Control (C++) and Convenience (python)
- Cross platform
- Ecosystem: 
  - Database (H2Database, Neo4J)
  - Big data (Spark)
  - AI (DeepLearning4J)
  - JSON parser (Jackson, GSON)

Workflow:

- write *java code* 
- compiled into *java bytecode* (java compiler)
- execute with *java virtual machine* (JVM)
- JVM + java compiler are part of java development kit (JDK)
- java bytecode are stored in `.class` files

Terminologies:

- **JSE**
  - Java Standard Edition
  - for desktop and standalone server applications
- **JEE**
  - Java Enterprise Edition
  - for running embedded in a java server

- **JRE**
  - Java Runtime Environment
  - JVM + Java APIs -> for running, but not compiling
- **Java SDK** == **JDK**
  - Java Software Development Kit
  - JRE + Java compiler

- Versions:
  - < Java 9: Java 1.X == Java X;
  - \>=Java 9: time-based, Java 9, 10, 11, ...
  - Java 8 and Java 11 (LTS) are the widely used versions
- Installation:
  - jdk is simply a zip file.
  - unzip and add  `/bin`  to the system path.



### Java Packages

Reference: https://docs.oracle.com/javase/tutorial/java/package/managingfiles.html

- Built-in packages: `math`, `util`, `lang`, `i/o`, `net`, and etc.

- User-defined packages: use the `package` keyword

  ```{java}
  package myPack;
  public class FirstProgram{
      public static void main(String args[]) {
          System.out.println("Welcome to myPack");
      }
  }
  ```

  - `package` must appear in the first line of each class
  - all classes should be placed in the folder with the same package name `myPack`
  - by convention a company uses its reversed Internet domain name for its package names, e.g. `com.ubs.mypack` , `org.apache.mypack`. The source files are organized in the same directory structure.

## `Maven`

### POM

Project Object Model

- https://maven.apache.org/guides/introduction/introduction-to-the-pom.html
- https://maven.apache.org/guides/getting-started/#how-do-i-make-my-first-maven-project
- defines related application versions, organize names
- defines the dependencies
- defines the build information (project structure,  plugins)

### Compile & Run

- `mvn clean` (clean the `target` directory)
  - This will remove the `target` directory with all the build data before starting so that it is fresh.

- `mvn compile` (compile source code)
  - maven will download all plugins and dependencies
  - compiled classes will be placed in `${basedir}/target/classes`
- `mvn test` (if there are unit tests)
- `mvn package` (create a jar file)
  - the jar file will be placed in `${basedir}/target`
- mvn install (install the generated artifact - the JAR file)

## Spring Boot

The `Spring` framework provides comprehensive infrastructure support for developing Java applications (e.g. JDBC, Security, Test, etc.). `Spring Boot` is an extension of `Spring`, but takes an opinionated view of the platform:

- Opinionated ‘starter' dependencies to simplify the build and application configuration
- Embedded server (e.g. Tomcat, Jetty) to avoid complexity in application deployment
- Automatic config for Spring functionality – whenever possible
- Support for the in-memory database such as H2.

`Spring Boot`  is  a microservice-based framework and makes production-ready applications in less time. The most important feature is Autoconfiguration. in `Spring Boot` everything is auto-configured. We just need to use proper configuration for utilizing a particular functionality. `Spring Boot` is very useful if we want to develop REST API.

On a higher level, spring follows and encourages basic `maven` directory structure.

To run your application, run the Application class that contains the main method that starts Spring Boot (run the main method directly)

### Project Structure

- Source Code: `src/main/java/`

- Test Code: `src/test/java/`
- Config files: `src/main/resources`
- Properties: `application.properties` 

