# React.js

React.js is a javascript library that is used to build **single-page** UI. It is used to build applications from reusable components and utilized the JSX (Javascript XML) language and virtual DOM concept. It uses Node.js to optimize javascript code and easy to create UI testcases



## Folder Structure

A thorough guideline can be found [here](https://saurabhshah23.medium.com/react-js-architecture-part-1-best-directory-folder-structure-2862de774eef).

```
my-app/
  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
  src/
    App.css
    App.js
    App.test.js
    index.css
    index.js
    logo.svg
```

A typical and boilerplate project structure follows the above hierarchy.

-  `public/index.html` must exist as the page template
- `src/index.js` must exist as the default JS entry point
- Only files in `public`/ can be used in `public/index.html`
- `package.json` is a manifest file of the application.
  - `package.json` = metadata of the app + all dependencies + scripts
- `webpack` will only see and process files in `src/`, therefore all `.css` and `.js` resources should be stored inside `src/`
  - `App.js` : the container of the app which serves the default entry point
  - `/assets`: static resources such as images, logos, and fonts
  - `/config`: app-level configurations, such as master datasets, date formats, etc..
  - `/components`: shared components, used across features/pages
  - `/pages`: cores of the application, individual screens/features each with its own `index.js` that exports the page container as the default module making it a functional component
  - `/services`: manage all api requests, the bridge between the database server APIs and the view layer (pages and components)

## App Architecture

An React app requires an ecosystem built around the core react js scripts. A middleware is present to handle different types of API requests and interface with various remote data sources -- a Tomcat server is a popular example. A state container (e.g. Redux) is introduced to handle app states consistently and enable undo/redo functionalities. A router for Single Page Applications (SPA) is required to map different URLs to different features/pages.

<img src="./images/react2.png" style="zoom:80%;" />

#### To Develop A React App

A nice overview of how to plan and proceed in react app development: https://reactjs.org/docs/thinking-in-react.html

1. start with UI design (graphical layout)
2. break down the different parts into a component hierarchy and define data model
3. build a static version of the app (components only have `render()` and uses `props`)
4. add interactivity (using `state`)
   1. determine a minimal set of variables that represent the app (mutable values that control the app behaviors and can not be computed from elsewhere)
   2. determine the owner of the state variables -- typically a parent component 
   3. pass callbacks from the parent component to child components to update (set) state variables.

## JSX

Most React developers use a special syntax called “**JSX**” which makes these structures easier to write.

JSX is similar to a template language (think of `jinjia` in `flask`), but JSX comes with the full power of JavaScript. You can put any JavaScript expressions within brackets (`{}`) in JSX. 

JSX produces React “elements”. Each **React element** is a JavaScript object that you can store in a variable or pass around in your program. You can put any valid JavaScript expression inside the curly braces ({expr}) in JSX
[intro to JSX](https://reactjs.org/docs/introducing-jsx.html)

## Elements
Elements are the smallest building blocks of React apps. An element describes what you want to see on the screen. Components are made of elements.

A component takes in parameters, called `props` (short for “properties”), and returns a hierarchy of views to display via the render method.

The `render()` method returns a description of what you want to see on the screen. React takes the description and displays the result. In particular, `render` returns a **React element**

## Components
Components let you split the UI into independent, reusable pieces, and think about each piece in isolation. They accept arbitrary inputs (called `props`) and return **React elements** describing what should appear on the screen.

Always start component names with a capital letter, otherwise it will be treated as a DOM tag by JSX such as `<div>`, `<li>`, etc..

All **React components** must act like pure functions with respect to their `props` (`props` should never be changed by functions).

Components has:

- `props` (parameters)
- `state` (internal state of a component, should always be updated via `this.setState()`, when the state changes, the component re-renders)
- `render()` (returns a React elements, to be rendered)

_NOTE_: Because this.props and this.state may be updated asynchronously, you should not rely on their values for calculating the next state. Instead, Pass a function to `this.setState()`, which takes the previous `state` and current `props` as input.

```{js}
incrementCount() {
  this.setState((state) => {
    // Important: read `state` instead of `this.state` when updating.
    return {count: state.count + 1}
  });
}
```

_NOTE_: a component should only update its own state. Therefore, to update a state from another component, a callback `setState()` should be passed from the other component to the current component to fire the operation. 

**Class component**: 
usually extends the `React.Component` class. 

- It has a `constructor`, in which `this.state` can be defined and `this.props` can be passed in. 
- It has a `render` method that returns what to display

**Function component**: 
component that only has a `render` method, no `state` (or the `state` is controlled by other components). Such components can be written as functions (taking `props` as input) instead of extending classes (`React.Component`)

**Controlled component**: 
a component fully controlled (state, callbacks, etc.) by another component, which passes `props` to the controlled component. This is usually achieved through **function components**


There are generally two approaches to changing data. The first approach is to mutate the data by directly changing the data values. The second approach is to replace the data with a new copy which has the desired changes. The latter is preferred, b/c:

- Complex Features Become Simple
- Detecting Changes is Easy
- Determining When to Re-Render in React is Easy


## Handling events	
- React events are named using camelCase, rather than lowercase.
- With JSX you pass a function as the event handler, rather than a string.
- it's important to bind callback functions in a class component, so `this` is correctly interpreted. An alternative is using arrow functions when passing the callback function.

```jsx
class LoggingButton extends React.Component {
  constructor(props) {
    super(props);
    ...
    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
    ...
  }
  
  handleClick() {
      this.setState(st => {
          return st.count += 1；
      })
  }
  render() {
    return (
      <button onClick={this.handleClick}>
        Click me
      </button>
    );
  }
}
```

`useEffects` executes the function inside it and has an optional second parameter, which is an array of properties to be observed within the scope of the stateless component. Whenever any of them are updated, the function is executed again.
`useState` for adding states in a stateless components
`import('path').then(func)` for using promises to import local data and store in a variable

