# JavaScript
It was invented in 1995, has multiple standards (ES5, ES6, ES2016, ES2017, etc.). Browsers and Node.js add more functionality through additional objects and methods, but the core of the language remains as defined in ECMAScript. 

Features:

- loosely typed

### `Node.js` vs. `React.js`

#### `Node.js`

Node.js is an open-source run-time environment for executing javascript outside browsers. It is used to build backend frameworks/services, e.g. API, WebApp. 

- It supports the Model–view–controller (MVC) framework.
- It handles requests and authentications from the browser and can make database calls.

#### `React.js`

React.js is a javascript library that is used to build **single-page** UI. It is used to build applications from reusable components and utilized the JSX (Javascript XML) language and virtual DOM concept. It uses Node.js to optimize javascript code and easy to create UI testcases.

- jsx: html mixed with JS, processed into javascript calls 
- virtual DOM: relies on the ReactDOM library, represents the UI virtually in memory and keep in sync with the real DOM (faster loading)
- React.js handles in-browser data and only makes API calls.

## Setup

[step-by-step guide](https://docs.microsoft.com/en-us/windows/dev-environment/javascript/nodejs-on-windows) 

Multiple node.js versions can be installed. To use a different version, switch into a project directory and run `nvm use <version>`

1. install `nvm-windows` (`nvm` is only supported on mac and linux) 
   1. download from [windows-nvm repository](https://github.com/coreybutler/nvm-windows#node-version-manager-nvm-for-windows)
   2. verify with `nvm ls`
2. install `node.js`
   1. `nvm list available`
   2. `nvm install <version>` 
   3. `nvm ls`
3. install `npm`
   1. comes with `node.js` installation
4. install `vscode` & [Node.js Extension Pack](https://marketplace.visualstudio.com/items?itemName=waderyan.nodejs-extension-pack)

5. start node.js
   1. `mkdir myProject`; `cd myProject`
   2. `nvm use <version>`
   3. write something in a script `app.js`
   4. execute by `node app.js`

1. install React
   1. use `create-react-app`: 

      1. `npx create-react-app myProject`

      				 2. NOTE: `npx` is a node package runner from `npm` to execute packages. It creates a temporary installation of React so that each project uses the most current React version and reduces the pollution of installing too many packages in one's local machine. 

      3. NOTE: if `npx` returns error due to space in user folder name, try: `npm config edit`  and update all "First Last" with "First~1"; or try: `npm config set cache "C:\Users\FIRSTNAME~1\AppData\Roaming\npm-cache" --global`

## Basics

### Comments
```js
// line comment

/* 
  block comment
*/
```

### Semicolons `;`
- `;` at the end of each line, although not a must in all cases
- The semicolon `;` is recommended at the end of statements, no matter what the value is
- The semicolon can be skipped at the end of code block `{...}`

### I/O
`console.log()` is the js equivalent of `print`


## Data Types
### Basic Data Types
- Number

- String

  - Template Literals

    - enclosed by ``

    - used to create multiline strings; handle html strings; use basic string formatting

    - ```js
      // all white spaces and new line breaks are part of the template literal
      let multi_line = `line 1 
      line 2`;
      
      // trim can help to remove the first empty line
      let html = `
      <div>
        <h1>Title</h1>
      </div>`.trim();
      
      // substitutions
      let name = "Tom",
          message = `Hello, ${name}.`;
      ```

  - Tagged Templates

    - used to apply a tag (function) to the subsequent string

    - ```js
      let s = String.raw`mutiline\nstring`;
      ```

- Boolean
  - `true` and `false`
  - evaluates to `false`: `0`, `""`, `undefined`, `NaN`, `null`

- Others:
  - `null`: value of nothing
  - `undefined`: absence of value
  - `NaN`: not a number

- `typeof`

- Symbols

```js
// Number
1 + 2 % 3 != 4;

// String
"Hello " + "World";
"Hello" + 5*10;
"Hello"[0] == "H";

// Variable
let postLiked = false;
let name = "Yvonne";

// others
let x = null;  // null ！= undefined
let x;         // undefined
let x = 5 / 0; // NaN
```

### Type Coercion
equality (`==`, `!=`) vs. strict equality (`===`, `!==`)

```js
'' == false; // true
0 == false;  // true
'1' == 1;    // true
'hello' + 1 == 'hello1';  // true
'1' === 1;   // false
0 !== false; //true
```
### Logical Operators:
- `&&`: AND
- `||`: OR 
- `!`:  NOT


## Conditionals
- `if...else` statements
- tenary operator: `conditional ? (if condition is true) : (if condition is false)`
- `switch` statements: without `break`, falling through behavior will take place

```js
// chained conditions
let weather = "sunny";

if (weather === "snow") {
  console.log("Bring a coat.");
} 
else if (weather === "rain") {
  console.log("Bring a rain jacket.");
} 
else {
  console.log("Wear what you have on.");
}

// tenary operator
let isGoing = true;
color = isGoing ? 'green' : 'red';

// switch statement
let option = 2;

switch (option) {
	case 1: 
		console.log('option 1 selected');
		break;
	case 2:
		console.log('option 2 selected');
		break;
	case 3:
		console.log('option 3 selected');
};


// make use of the falling through behavior
let month = 2;

switch(month) {
  case 1:
  case 3:
  case 5:
  case 7:
  case 8:
  case 10:
  case 12:
    days = 31;
    break;
  case 4:
  case 6:
  case 9:
  case 11:
    days = 30;
    break;
  case 2:
    days = 28;
};
```

## Loops
- `while` loops
- `for` loops
    - `for ... of`: returns a list of values of the numeric properties of the object being iterated
    - `for ... in`: returns a list of keys on the object being iterated
    - `.forEach()`
- increments/decrements
	- `x++`, `x--`: return value of x before the change, then apply the change
	- `++x`, `--x`: apply the change, then return value of x after the change, 
	- `x *= 2`, `x /= 2`

```js
let start = 0; 
while (start < 10) {
  console.log(start);
  start = start + 2;
};

for (let i = 0; i < 6; i = i + 1) {
  console.log("Printing out i = " + i);
};
```
loop over data structures: `Array`, `Map`, `Set`, `Object`

```js
let list = [4, 5, 6];

for (let i in list) {
   console.log(i); // "0", "1", "2",
}

for (let i of list) {
   console.log(i); // "4", "5", "6"
}
```

## Scopes
- Global scope: variables are visible **anywhere** in the code
- Function scope: variables are visible within **a function**, a.k.a. `Local` 
- Block scope: variables are visible within **a `{ ... }` block**
- Define Variables
	- `let`: new way of defining variables, in any scope.
	- `const` defining constant (immutable) variables, in any scope.
	- `var`: old way of defining variables, only in Global and Local scopes

## Functions

### Basics

- `undefined` is returned by default
- `arguments` is a built-in array that stores all input arguments
- `...` : rest parameter, same as `*args` in python; introduced to replace `arguments`

```js
function findAverage(x, y=2) {
  let answer = (x + y) / 2;
  return answer;
}

function findSum(x1, ...args) {
    let s = x1;    
    for (let i = 0; i < args.length; i++) {
        s += args[i]
    };
    return s;
}
```
### Hoisting

##### functions

Functions and variable declarations (not variable assignments) are hoisted to 
the top of the script before execution. So functions can be called before 
they are declared.

To avoid the confusions, always write function declarations at the top of a script,
and declare variables at the top of a function.

##### variables

Variable declaration behaves similarly. `var`  offers scope hoisting; but `let` limits the scope to the current block (`{...}`) only. `let` is preferred after JS 8

variables can not be redeclared in the same scope, but  can be updated.

### Using the `new` Constructor

- A function can be called with our without `new`  -- the dual purpose of js functions.
  - JavaScript has two different internal-only methods for functions: `[[Call]]` and `[[Construct]]`. When a function is called without `new`, the `[[Call]]` method is executed, which executes the body of the function as it appears in the code. When a function is called with `new`, that’s when the `[[Construct]]` method is called
- When called with `new` , a new instance of the function will be created, `this`  will contain the new instance and be returned, if no object is explicitly returned. This is similar to `class` objects and `self` in python.

### Using `apply()` 

TODO

### Function Expressions

a.k.a anonymous functions. Such functions are not hoisted, b/c they involve variable assignments.

```js
let catSays = function(max) {
  let catMessage = "";
  for (let i = 0; i < max; i++) {
    catMessage += "meow ";
  }
  return catMessage;
};

// common practice: pass a function into another function inline
function movies(messageFunction, name) {
  messageFunction(name);
}

movies(function displayFavorite(movieName) {
  console.log("My favorite movie is " + movieName);
}, "Finding Nemo");
```

### Arrow Functions
another way of defining functions: `let func = (arg1, arg2, ..., argN) => expression`

- Do not have `this`. (`this` is looked up from the outside, useful for defining functions inside a method of an object)
- Do not have `arguments`. (`arguments` is used in decorators to pass arguments from the decorated function)
- Do not have `prototype`. (`prototype` is used to update the original function's properties after it's defined)
- Can’t be called with `new`. (can not be used as constructors)

```js
let sum = (a, b) => a + b;
let sayHi = () => {console.log('Hello!')};
```

## Array
Arrays can store anything. Indexing out-of-bound elements will return `undefined` 

```js
let arr = [];
let mixedData = ['1', 2, [3, 4], true];

let colors = [ "red", "green", "blue" ];
let [ firstColor, secondColor ] = colors;
```
### Properties and methods
- `arr.length`
- `arr.push/pop()`; `arr.reverse()`; `arr.sort()`
- `arr.forEach()`: very useful
- `arr.map()`: very useful

```js
let donuts = ["jelly donut", "chocolate donut", "glazed donut"];

let improvedDonuts = donuts.map(function(donut) {
  donut += " hole";
  donut = donut.toUpperCase();
  return donut;
});

donuts.forEach(function(donut) {
  donut += " hole";
  donut = donut.toUpperCase();
  console.log(donut);
});
```

### Iterators and Generators

TODO

## Map and Set

### Map

`Map` is a collection of keyed data items, just like an `Object`. But the main difference is that `Map` allows keys of any type, while `Object` converts keys into strings. Using objects as keys is one of the most notable and important Map features. **`map[key]` isn’t the right way to use a `Map`**, instead we should use `set()`, `get()`, etc.

- `new Map()` – creates the map.
- `map.set(key, value)` – stores the value by the key.
- `map.get(key)` – returns the value by the key, undefined if key doesn’t exist in the map.
- `map.has(key)` – returns true if the key exists, false otherwise.
- `map.delete(key)` – removes the value by the key.
- `map.clear()` – removes everything from the map.
- `map.size` – returns the current element count.

```js
let map = new Map();

map.set('1', 'str1');   // a string key
map.set(1, 'num1');     // a numeric key
map.set(true, 'bool1'); // a boolean key
```

### Set

each value may occur only once. Iterations over `Set` can utilize `for ... of` or `forEach()`

```js
let set = new Set(["oranges", "apples", "bananas"]);

for (let value of set) alert(value);

// the same with forEach:
set.forEach((value, valueAgain, set) => {
  alert(value);
});
```

## Objects

Objects are a data structure in javascript that can be used to store anything and the data stored can be accessed via a **key**

```js
let user = new Object(); // "object constructor" syntax
let user = {};  // "object literal" syntax

let umbrella = { 
  color: "pink",
  isOpen: false,
  open: function() { 
    if (umbrella.isOpen === true) {
      return "The umbrella is already opened!";
    } else {
      umbrella.isOpen = true;
      return "Julia opens the umbrella!";
    }
   }
};

umbrella['color'] == umbrellad.color;
umbrellad.open();

let node = {
        type: "Identifier",
        name: "foo"
    };
let { type, name } = node; // object destructuring
```

### Computed properties:

```js
let fruit = prompt("Which fruit to buy?", "apple");

let bag = {
  [fruit]: 5, // the name of the property is taken from the variable fruit
};

alert( bag.apple ); // 5 if fruit="apple"
```

### `this`
`this` is similar to `self` in python, however it's not bound. The exact reference `this` points to is evaluated at run time, according to the scope it's in. `this` must be called within a object, otherwise it will return `undefined`. Be careful about what `this` is pointed to in different contexts/scopes.

```js
let user = {name: 'John'};
let admin = {name: 'Mike'};

function sayHi() {alert(this)};

user.sayHi = sayHi;
admin.sayHi = sayHi;

user.sayHi();  // this == user
admin.sayHi(); // this == admin
```

## Class

js didn't officially support class until ECMAScript 6. Many still believe the language does not need classes. Before ECMAScript 6, defining a function constructor and adding methods to its prototype is the typical way to define a class. 

- `new` is used to construct new instances
- `construct()` is similar to `__init__()` in python
- `prototype`

```js
class MyClass {
  // class methods
  constructor() { ... }
  method1() { ... }
  method2() { ... }
  method3() { ... }
  ...
}
```

### Class Expression

Similar to function expressions, class expressions are used to define anonymous classes and be passed directly as arguments 

### Accessor Properties

Similar to python, setters and getters can be defined for internal properties. Use the `get` and `set` keywords.

```js
class CustomHTMLElement {
    constructor(element) {
        this.element = element;
    }
    get html(){
        this.element.innerHTML;
    }
    set html(value){
        this.element.innnerHTML = value;
    }
}
```



## Asynchronous Programming

#### Previous Approach

JS uses a single-threaded *event loop*. A *job queue* is maintained to keep track of code pieces to be executed. Traditional asynchronous execution is achieved through the **event model** or **callback pattern** in JS. Both register a piece of code (or nested code pieces) with an event, which is pushed to the *job queue* to be executed at a later time. Two examples below:

```js
// Event Model (subscription)
let button = document.getElementById("my-btn");
button.onclick = function(event) {
    console.log("Clicked");
};

// Callback Pattern (callback function)
readFile("example.txt", function(err, content){
    if (err){
        throw err;
    }
    
    console.log(contents);
});
```

#### Promise

Promise is a placeholder of the result of an asynchronous operation. The function returns a promise object, the content of which will be filled in the future. A promise object has 3 states: `pending`, `fufilled`, `rejected` .



A new promise is created by using the `Promise` constructor. The constructor accepts a function called *executor*, which is passed two arguments `resolve` and `reject`. The *executor* is executed immediately, while `resolve()` is only executed when the promise is settled otherwise`reject()`is executed. Whenever `resolve()` or `reject()`is executed, a job is added to the job queue to resolve the promise.



All promises are *then-able*, meaning that `promise.then()` can be used to listen to fulfillment or rejection operations and handle each situation. `then()` can be chained and each returns a new promise when the previous one is resolved. Chaining promises can pass data from one to the next.

```js
let fs = require("fs");

function readFile(filename) {
    return new Promise(function(resolve, reject) {

        // trigger the asynchronous operation
        fs.readFile(filename, { encoding: "utf8" }, function(err, contents) {

            // check for errors
            if (err) {
                reject(err);
                return;
            }

            // the read succeeded
            resolve(contents);

        });
    });
}

let promise = readFile("example.txt");

// listen for both fulfillment and rejection
promise.then(function(contents) {
    // fulfillment
    console.log(contents);
}, function(err) {
    // rejection
    console.error(err.message);
});
```

## Modules

- module code always runs in strict mode
- code pieces must be exported to become available externally. 
- the value of `this` in the top level of a module is `undefined`

#### Export

One can only set one `default` export per module. the `default` export does not need a name, b/c the module itself represent the exported object.

```js
// export data
export let color = "red";

// export class
export class Rectangle {
    constructor(length, width) {
        this.length = length;
        this.width = width;
    }
}

// define a function...
function multiply(num1, num2) {
    return num1 * num2;
}

// ...and then export it later
export { multiply };
// or rename it
export { multiply as multi };
// or make it a default
export { multiply as default }
```

#### Import

Importing default values does not require `{}`. The imported name can be arbitrary to represent the default export from the module.

```js
// include extension for local files; packages do not need file extensions
// include "./" or "../" for best compatibility.
import { identifier1, identifier2 } from "./example.js";
import { identifier3 as id3 } from "example.js"
import default_identifier from "example.js"
```



## Strict vs. Non-strict Mode

TODO

# DOM Tree

A very nice explanation at [javascript.info](https://javascript.info/dom-nodes).

The backbone of an HTML document is tags. DOM (Document Object Model) is used to model and decompose an HTML document. Every html tag is an object, and the nested tags are children objects. The text inside the tag is also an object.

Tags are *element nodes* (or just elements) and form the tree structure. The text inside elements forms *text nodes*, labelled as `#text`. A text node contains only a string. It may not have children and is always a leaf of the tree.
