1. All HTML documents must start with a type declaration: <!DOCTYPE html>.
2. The HTML document itself begins with <html> and ends with </html>.
3. The visible part of the HTML document is between <body> and </body>.
4. HTML documents are made up by HTML elements, HTML elements are written with a start tag, with an 	end tag, with the content in between. Some HTML elements do not have an end tag. HTML elements 		can be nested (elements can contain elements). HTML tags are not case sensitive.
5. Attributes are used to provide additional information about HTML elements. Attributes are always 	specified in the start tag
6. The HTML <head> element only contains meta data. The HTML <head> element is placed between the 		<html> tag and the <body> tag. Under <head>, we can have <title>, <meta>, <style>.   </>
7. Every HTML element has a default style. Changing the default style of an HTML element, can be done 		with the style attribute. style="property:value"
8. Every visible HTML element has a box around it, even if you cannot see it.
9. Comments are formatted as <!-- Write your comments here -->


HTML headings are defined with the <h1> to <h6> tags, use<h1-6> only for headings, not enlarging texts
<h2>This is a heading</h2>

the <br> tag defines a line break

the <hr> tag creates a horizontal line in an HTML page

HTML paragraphs are defined with the <p> tag
e.g. <p title="paragraph">This is a paragraph.</p>

The HTML <pre> element defines a block of pre-formatted text, with structured spaces and lines

HTML links are defined with the <a> tag
e.g. <a href="http://www.w3schools.com">This is a link</a> 

HTML images are defined with the <img> tag
e.g. <img src="w3schools.jpg" alt="W3Schools.com" width="104" height="142"> 

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
HTML Text:

Any number of spaces, and any number of new lines, count as only one space.Browser decides how to display. 

"style" attributes include: 'color', 'font-family', 'font-size', 'text-align'

text formatting is done by using special elements, for defining text with a special meaning.
<b>, <i>, <strong>, <em>, <mark>, <ins>, <sub>, <sup>, <small>

<q> for short quotations; <blockquote> for long quotations

<address>, <bdo>, <cite>, <dfn>

<kbd> for keyboard input; <samp> for computer output sample; <code> for programming code, better to use under <pre> to preserve indentation and line breaks

<var> for mathematical variables.

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
HTML Style with CSS (Cascading Style Sheets):

styles can be added to elements in three ways:
    Inline - using a style attribute in HTML elements
    Internal - using a <style> (</>) element in the HTML <head> section
    External - using one or more external CSS files (recommended)

CSS styling has the following syntax:
element {property:value; property:value}

External style sheet are ideal when the style is applied to many pages. You can change the look of an entire site by changing one file. This is defined under the <head> section in the <link> element.
e.g. <head> <link rel="stylesheet" href="styles.css"> </head>

To define a style for a special element, first add id to the element and then define the style.
first:	<p id='p01'>special one </p>
then:	p#p01 {color: blue}

to define a style for a special type of elements, first add class to the elements, then define the style.
first:	<p class='different'> special class </p>
then:	p.different {color: blue}

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
HTML links:
<a href='URL'> Visible Text </a>

Links can be element, icon, text, image, etc that can be clicked on.

Local links are specified with a relative URL, without the 'http://.....'

styles of the links, when the mouse is over or clicked can be changed under <head> in <style> </>:
<style> { a:link {color:#000000; background-color:transparent}
a:visited	{}
a:hover 	{}
a:active	{} } </style>

The target attribute specifies where to open the linked document
target = '_blank', '_self', '_parent', '_top', framename

The id attribute creates bookmarks inside the HTML document, so that other parts of the document can redirect to the part with id.
<a id='#tips'>Useful Tips Section</a>
<a href='#tips'> Visit the tips section</a> <!--in the same page-->
<a href='URL/link.html#tips'> Visit the tips section</a> <!--from another page-->


