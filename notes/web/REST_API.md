# REST API

Representational state transfer (REST) or RESTful Web services.
- It is one way to provide interoperability between computer systems on the internet.
- It takes advantage of HTTP methodologies, networked components are a resource one requests access to.
- It breaks down a transaction into small modules, that are completely stateless, meaning that handling each module is entirely based on the - information that comes with it.
- It is therefore, re-deployable and scalable.
- It is preferred in web use, especially in cloud computing and microservices


## Appendix:
### Microservice
- modular approach to application development, where a large application is made up of small modules that run in their own process and database.
- It enables faster building/compiling process if only certain modules are changed within the application.
- It also has the advantage of re-usability and scalability, and works well with containers such as Docker.
- Drawbacks includes: too granular, latency in heavy use, complex testing
