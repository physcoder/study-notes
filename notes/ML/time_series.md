# Time Series

Any ‘non-seasonal’ time series that exhibits patterns and is not a random white noise can be modeled with ARIMA models.

An ARIMA model is characterized by 3 terms: p, d, q
where,

- p is the order of the AR term
- q is the order of the MA term
- d is the number of differencing required to make the time series stationary

If a time series is not stationary, then preprocessing is needed to make stationary. 
If a time series has seasonal patterns, then you need to add seasonal terms and it becomes SARIMA, short for ‘Seasonal ARIMA’.

[Reference](http://people.duke.edu/~rnau/whatuse.htm)
### ACF vs. PACF
- ACF:
	- ACF measures the correlation of an observation and an observation at a prior timestamp (the former's lag), including both direct and indirect dependencies (correlations with intervening observations)
	- ACF for an `AR(k)` process would be strong up to lag `k`, the inertia will carry on to subsequent lags, but will gradually weaken and trail off eventually.
	- ACF for an `MA(k)` process would be strong up to lag `k`, but with a sharp decline to low/no correlation beyond.
- PACF:
	- PACF measure the correlation of an observation and an observation at a prior timestamp (the former's lag), with the indirect dependencies removed
	- PACF for an `AR(k)` process will be zero beyond lag `k`. 
	- PACF for an `MA(k)` process will have a strong relationship to the lag and a trailing off of correlation from the lag onwards (b/c MA already does not trend/seasonality/etc.)

### Differencing
both AR and MA models are linear regression models.

linear regression requires variables to be independent, so the raw time series has to be processed to be stationary. One way to do it is differencing (d), by subtracting previous values from current values. d determines the order of differencing (1st, 2nd, 3rd, ... order derivatives). If the time series is already stationary, then d=0

$$
\begin{align}
& x_t = X_t,   &if d = 0 \\
& x_t = X_t - X_{t-1},  &if d = 1 \\
& x_t = (X_t - X_{t-1}) - (X_{t-1} - X_{t-2}), &if d=2 \\
\end{align}
$$

check if the series is stationary using the Augmented Dickey Fuller test (null hypothesis of the ADF test is that the time series is non-stationary), reject with small p-values (go with differencing)

look at the ACF plot after differencing, the ACF values should reaches zero pretty quickly, and the time series should roam around a defined mean. If the autocorrelation stays positive for many number of lags (>10), then the data needs further differencing. Use the minimum d that makes the data stationary.

If your series is slightly under differenced, adding one or more additional AR terms usually makes it up. Likewise, if it is slightly over-differenced, try adding an additional MA term

### AR(p) (Auto Regression)
a linear regression where the lags of the data itself are used as predictors. Autocorrelation measures correlation coefficients between two sequences of data separated by certain time periods.

AR model:
$$ x_{t} = \alpha + \beta_1 x_{t-1} + \beta_2 x_{t-2} + ... + \beta_p x_{t-p} + \epsilon_1$$

determine p:  
look at PACF (partial autocorrelation) plots and find p where the partial autocorrelation value is near the significance line. PACF of a particular lag is essentially the coefficient on that lag in the AR equation 

- A model with no orders of differencing normally includes a constant term
- A model with two orders of total differencing normally does not include a constant term
- A model with one order of differencing

### MA(q) (Moving Average)
a linear regression where the errors from the AR forecasting errors are used as predictors (to help correct future forecasts). 

MA model:
$$ x_{t} = \alpha + \epsilon_t + \phi_1 \epsilon_{t-1} + \phi_2 \epsilon_{t-2} + ... + \phi_q \epsilon_{t-q}$$

determine q:  
use ACF plot to determine q where the auto correlation value is near the significance line.

### Simple Moving Average
When data are in short supply and/or highly irregular. can be robust against outliers, but long-term forecast would just be a flat line, extracted from most recent data.

### ARIMA (p, d, q)
adding AR and MA models together. When data are relatively abundant. it may not work very well on disaggregated, sparse, ireegular data.

ARIMA model:
$$ x_{t} = \alpha + \epsilon_t +\sum_{i=1}^{p}\beta_i x_{t-i} + \sum_{i=1}^{q}\phi_i \epsilon_{t-i}$$

- Random Walk: 
	- ARIMA(0,1,0): $x_t = \epsilon$
	- a baseline model
- ARMA:
	- ARIMA(p, 0, q): only for stationary series


### SARIMA
it uses seasonal differencing and adds additional 3 parameters. So in stead of differencing on consecutive terms, differencing on previous seasons (pre-defined seasonality). The model can be represented as $SARIMA(p,d,q)m(P,D,Q)$, where P, D, Q are the values for SAR, SD, SMA, and m is he number of time steps for a single seasonal period. 

As a general rule, set the model parameters such that D never exceeds one (no more than 1 order of season). And the total differencing $d + D$ never exceeds 2. Try to keep only either SAR or SMA terms if your model has seasonal components.


### SARIMAX
incorporating exogenous (external) variable X (e.g. market indices, macro trends). Need to know X's values at all times (training and testing)

### ARCH
autoregressive conditional heteroskedasticity

### Evaluate:
##### data split:
- conventional train-test split (find a split point)
- multiple train-test splits (constant test size, with an increasing training size)
- most robust way: walk-forward validation (define a min-sample size or window width, start with the window size, train model & predict next & store prediction, then expand the window size by 1, repeat the process)

##### performance:
- residual plot, Q-Q plot, 
- p-values of coefficients as a guideline for tuning p, q
- out-of-time validation (true vs. predicted)
- metrics: MSE, RMSE, MAE, MAPE AIC, BIC, etc.


### Prophet Package
- treat time series modeling as a curve fitting process.
- additive regressive model: g(t) + s(t) + h(t)
	- g: growth trend (saturated growth, etc. takes on different functional forms)
	- s: seasonality (sine/cosine)
	- h: holiday effects (changepoints)
- fast, versatile, and interpretable