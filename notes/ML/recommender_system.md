# Recommender Systems
https://towardsdatascience.com/introduction-to-recommender-systems-6c66cf15ada

## Data
- User charateristics
  - preferences
  - demographics
- Product characteristics
  - descriptions
  - catalog info
- User/Product interactions
  - purchases
  - ratings
  - likes
  - browsing history
- Derived relationships
  - product similarities
  - user similarities


## Similarity measures
- `p`-norm (Minkowski distance)
  - L1 norm (Manhattan distance)
  - L2 norm (Euclidean distance)
- Cosine Similairty
  - measures angles rather than magnitudes, between two vectors
- Pearson Coefficients
  - measures covariance, between two random variables
- Jaccard Similarity
  - measures overlap, between finite sets (e.g. top 5 favorite items)
- Hamming Distance
  - measures difference between categorical variables (bits in binary data strings)


## Approaches
### Typical challenges:
- highly sparse user-product interactions
  - large dimensions (# of users and # of products) 
  - large dimensions (interactions can be multi-dimensional)
  - limited data (small amount of user post ratings)
- generalize to products that is outiside users' historic interests
- temporal effects lead to constant retraining/learning

### Content-based
Using product charateristics and/or user charateristics (content info), (1) such system constructs user profiles of like/dislike about products (e.g. TF-IDF vectors); It then makes recommendations to users based on their profiles. (2) such system can also build item profiles, and then measures similarities between products and make recommendations based on users past interactions. In either case, the profiles are directly built from given features, not learned from interactions.
- Methods: classification/regression methods can be reformulated to map feature vectors to interaction values.
  - item-centered: model for each item.
  - user-centered: model for each user.
- Pros:
  - intuitive and simple
  - 'cold start' is less of a problem
- Cons:
  - can not recommend complementary products outside user charateristics

### Collaborative Filtering
Use only user-product interactions to filter for item of interests. The ultimate goal is to predict unknown interactions in the user-product matrix from known ones.
- Memory-based: work directly with past user-item interactions, measure similarities between users or products, and leverage nearest neighbor searches without latent model assumptions. This approach does not scale well.
- Model-based:  learn the underlying 'generative' model that explains the interactions.
- Cons: faces the 'cold start' problem

##### Matrix Factorization
learn U and I from known R. U and I represents the latent space where users and items are described by reduced but dense dimensions.
> - `R` = user-item rating matrix
> - `U` = user matrix (user representation)
> - `I` = item matrix (item representation)
> - `R  = U*I`
> - `argmin_{U, I} ||U*I-R||^2`

##### Neural Network
Use NN to find `U` and `I`:
> - input is the concatenated `[u, i]`, for each `(u, i)` interaction
> - output is the `(u, i)` interaction values (e.g. ratings)
> - learn the representation (`[u, i]`) and the NN weights at the same time
> - metric can be MSE (real valued interactions) or cross-entropy (binary/nominal interactions)

##### item2vec
representation learning

### Hybrid
combine content-based and collaborative filters together 


## Evaluation
- Online: A/B testing, collect interaction metrics (clicks, conversion rates, etc.)
- Offline: quantize matches on test data


## Design a System
- look at the data
  - If: only meta data on products;       Then: start with content-based approach 
  - If: large enough data on interaction; Then: try collaborative filtering 
- define interactions
  - explicit interactions: rating, review, etc.
  - implicit interactions: search, browse, etc.
- communication to the users:
  - included several recommendations than just one
  - provide explanations for recommendations, such as 'users who love this also love...'


