# Hyper-parameter Tuning



## Popular Packages

- `hyperopt`& `hyperopt-sklearn`
- `scikit-optimize`
- `optuna`
  - Pythonic way of defining search space
  - Framework-agnostic
  - Built-in visualization and parallelization



## Methods

- Manual Search

- Grid Search

  - automated manual search

- Halving Grid Search

  - binary-search-like grid search, faster than Grid Search

- Randomized Search

- Halving Randomized Search

  - binary-search-like randomized search, faster than Randomized Search

- Bayes Optimization

  