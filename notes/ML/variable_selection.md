# Variable Selection

### Filtering methods:
- correlation
- chi-squared test
- mutual information

### Recursive Feature Elimination:
similar to step-wise selection, but not p-value or R-squared based

- use some kind of scores
    - AIC
    - BIC

### Built-in 
- trees: feature importance
- lasso regression 

### Tips
- use different datasets for model selection and feature selection
- or during cross validation, do feature selection in each fold, right before model training, so feature selection is part of model selection
- for stability, use bootstrap for model averaging