# Linear Regression

## Assumptions
- linear relationship
- independent observations
- independent variables
- errors follow zero mean Gaussian with constant variance (weighted LS for non-constant variance)

With a squared loss function, ordinary least square regression is using linear function to estimate E(Y|X), which is the best prediction under this loss. Nearest Neighbor based regression directly approximate E(Y|X), but only using local information, the overall structures in the data set is not utilized. Under high dimensions, the data sparsity/ non-uniform sampling makes N.N. methods insufficient.

Ordinary least squares solution gives the best estimate for the coefficients among all **linear unbiased** estimators. However, the expected prediction error consists of both bias and variance, other slightly biased estimators may have much smaller variance resulting in better prediction error, e.g. adding regularizations, subset regression, step-wise regression, ...

When error term is not Gaussian, instead is fat-tailed (pareto or cauchy) in finance, one has to go back to the origin using the new distribution function to construct the joint likelihood and do MLE.

#### Extensions
4.3.7 in the [Interpretable ML Book](https://christophm.github.io/interpretable-ml-book)

## Multi-collinearity
Multicollinearity increases the standard errors of the coefficients. Increased standard errors in turn means that coefficients for some independent variables may be found not to be significantly different from 0.

However, multi-collinearity does not affect predictions, or how well the model fits. If the model is only used for prediction, then multi-collinearity is not a problem. (only when they are perfectly correlated, I think)

### Signs of collinearity:
- a theoretically predictive variable appears to be not significant;
- add or delete a variable, the regression coefficients change dramatically;
- negative regression coefficients while they should be positive.

### Solving collinearity:
- remove correlated variables (correlation coefficients, VIF)
- PCA, LDA
- adding regularization
- normalize variables by subtracting mean (works well for higher polynomial order terms derived from main variables)
- check dummy variables (if every category has a dummy and a constant offset (beta0) is also added, perfect collinearity is guaranteed)

### VIF: Variance Inflation Factor
The term measures how much the variance of an estimated regression coefficient (of a variable) would increase from collinearity with all other variables. The variance of an regression coefficient is:
$$ \hat{Var(\hat{\beta_i})} = \frac{s^2}{(n-1)\hat{Var(X_i)}} \frac{1}{1-(R_i)^2} $$
$s$ is the RMSE of the overall data set; $\hat{\beta_i}$ is the estimated coefficient of variable $X_i$; $(R_i)^2$ is the multiple R-squared for regressing $X_i$ on all other variables, excluding the dependent variable $Y$.

So VIF is defined as the additional term for $\hat{Var(\hat{\beta_i})}$ and it is:
$$ \frac{1}{1-(R_i)^2} $$
VIF equals to 1 when the variable is orthogonal to all other variables, and will be bigger than one when it is correlated with others. Thus the square root tells how many times the variance of the regression coefficient estimate would increase due to collinearity than without. generally VIF>5 is considered high multi-collinearity, VIF>10 is extremely high.

### Correlation Coefficients
Pearson: linear correlation  
Spearman: rank ordering correlation
Cramer's V: correlation between categorical variables

## Feature Engineering
- Interaction terms
- Polynomial terms
- Variable Transformation
  - box-cox transformation: stabilize variance, more normal-like
  - log: income/ housing price
  - **normalize**: should usually be done before modeling -> better interpretability of intercept, coef all on the same scale (varImp)
  - binning
  - imputation
  - polynomials: more flexible relationship
- Non-linearity
  - variable transformation;
  - adding transformed variables;
  - Generalized Linear Model;
  - Support vector regression using kernel functions (dual form, set derivative of loss to zero, substitute solutions back in loss)

## Partition of Variance
$total variance = explained + residual$
- This is only true in the context of least squares. In other words, $R^2$ may not be between 0 and 1 if such assumptions are violated.
- Proof: [Wikipedia](https://en.wikipedia.org/wiki/Partition_of_sums_of_squares)

## R-squared vs. R-squared(pred) vs. adjusted R-squared vs. P-value vs. RMSE
- **R-squared** or **multiple R-squared** 1- (residual squared)/(total data variance); measures percentage of variability in the data explained by the model; it is a biased estimate of population R-squared; R-squared does not tell estimate bias (residual plot), does not tell model is adequate or not (more variables needed). R-squared always increases as more variables are added.
- **adjusted R-squared** looks at number of variables in the model and would increase only when a new term improves the model more than by chance; always lower than R-squared and can be negative; it is an unbiased estimate of the population R-squared
- **R-squared(pred)** measured percentage of variability the model explains in predicting future observations (overfitting); calculated by systematically removing each observation in the model; it can be negative (meaning the model is not good)
- **RMSE** is a common accuracy measure, how close predictions are to the true values.
- **P-value** measures the lowest level (probability) at which one can still reject the null hypothesis, usually in regression being the coefficient equals to zero. The smaller the better
- **Z-score** measures how far away the estimated coefficients are from zero, in units of its variance.
- high R^2 and low P-value indicates a good model; but low R^2 and low P-value can still mean a valid model, meaning that there is more variability in the prediction (wide prediction interval) due to more noise in the data or missing of other important variables or time-dependent information in the model; high R^2 does not necessarily mean a good model, b/c there might be systematic bias in the prediction or overfitting to the data, looking at residual plot is a good idea.

## Residual plot
Plotting residual vs. predicted values for each observation.  
Residuals should show no clear pattern, but with a similar spread centered around zero  
Plotting residuals vs. row number. (time series plot testing autocorrelation)

## Variable/Model Selection
- Goal: overfitting/underfitting, collinearity, computation cost(complexity)  
  looking at training performance:  
    quantify performance: what criterion (AIC, BIC, R^2, adjusted R^2)  
    method to select: how to search (find all subsets, one at a time->forward/backward selection)  
  looking at validation performance:  
    cross validation  
  looking at model complexity  
    regularization (lasso for correlation as well)  
- Respect the hierachy in the models ($x$ vs. $x^2$; $xy$ vs. $x^2$ or $y^2$); remove higher order terms first; remove same order terms together
- Dummy variables from one categorical term need to be tested together (all levels), F-test is usually used. Can also do *grouped lasso*
- Criterion
    + AIC: measures the **relative** quality (information loss) of a collection of models for a given data set, the preferred model is the one with minimum AIC value. It rewards goodness of fit but also penalizes high complexity.
        +  Definition: maximum likelihood function L and number of parameters k; aic = -2log(L) + 2k
    + BIC: closely related to AIC and also prefers model with smaller BIC values. It assumes the data distribution (p(x|M)) is in an exponential family. BIC tends to favor simpler models than AIC.
        +  Definition: maximum likelihood function L and number of parameters k and sample size N; bic = -2log(L) + k*log(N)
    + Adjusted R-squared: account for number of variables
- Model Selection methods:
    + Subset selection
        + Stepwise regression: selects models by adding or removing variables one step at a time and returns the best model that contains 1, 2, ..., k predictors.
        + Best subsets regression: compares **all** possible models using a specified set of predictors, and displays the best-fitting models that contain 1, 2, ..., k predictors.
    + Shrinkage methods (p71 - ESL)
        + Ridge: L2, no regularization on the intercept. It adds a term to the $X^T \cdot X$ inverse, making it more robust. It shrinks small principle components more
        + Lasso: L1, no regularization on the intercept. It can make certain terms zero. No closed form expression.
            + LAR: least angle regression, tests the correlation between X and residual in each iteration, move the regression coefficients gradually to update the residual till another variable catches up. Provides an efficient way of calculating Lasso paths. It finds both high variance and high correlation to Y subsets, rather than PCR
            + Coordinate Descent: holding other variables fixed and tune one, a good way of Solving Lasso.
        + L0: number of non-zero coefficients, gives sparsity; but it is not convex, nobody uses it.
    + Cross Validation: balance of training and test performance; also used to determine k for the output from subset selection.  
    + F-test/ Chi-squared test: goodness of fit

### Other regression methods:
- Subset selection
    + Best Subset Regression: identifies a useful subset of predictors based on how much variation the model explains (the maximum R-squared criterion)
    + Stepwise Regression: forward/backward selection based on chosen critical value alpha. forward selection provides low variance; faster than backward selection; can be trained sequentially making use of QR decomposition; works in both N > d and N < d cases 
    + Forward-Stagewise Regression: every step, it looks at the variable that is least correlated with the current residual, and add it to the model, until all left features are uncorrelated with the current residual. It is slow to train, but works well in high-dimensional problems.
- Regression by Successive Orthogonalization: using Grand-Schmidt procedure, construct orthogonal predictors before estimating the coefficient for a variable.
- Using derived inputs
    + Principal Components Regression: choose subset directions that maximize the variance
    + Partial Least Squares: choose subset directions that maximize both variance and correlation with the response Y.
    + PCR and PLS show similar parameter tuning trend with Ridge, although more discrete and extreme.

### Regression with categorical response (Y)
This is in fact called classification in machine learning.
- Binary logistic regression: when response is Y/N (0/1)
- Nominal logistic regression: when response has rank ordering (less, more, most)
- Ordinal logistic regression: when response does not have an order and has more than two levels (chicken, duck, bird)

## Q-Q plot
quantile-quantile plot, can examine similarity between two distributions
