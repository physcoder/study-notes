## Data Structures:
- Series: a 1D labeled array, dictionary like
    - capable of holding any data type; acts like ndarray, compatible with most Numpy methods;
    - automatically aligns labels when numeric operations are done between two Series.
- DataFrame: a 2D labeled data structure,
    - a collection of Series organized in a dictionary format
    - `column` parameter will override keys in the dict data passed in.
    - automatically aligns both row and column labels when numeric operations are done between two DataFrames
- Panel: 3D data, a collection of DataFrame
    - three axes: `item`, `major_axis`, `minor_axis`
    - `item`: axis0, each item is a DataFrame
    - `major_axis`: axis1, the row index of each DataFrames
    - `minor_axis`: axis2, columns of each DataFrames
- Index: axis labels (both row and column)


## Series Operations:
- name of Series: `s.name`; `s.rename('new_name')`
- sort Series:    `df['col'].sort_values()`
- unique values:  `df['col'].unique()`
- map values:     `df['col'].map(func)`
- text data:      `df['col'].str.`;
                  `df['col'].str[0]`;
                  `.str.findall('pat')`; `.str.match('pat')`; `.str.replace('pat', repl)`; `.str.contains('pat')`
- `Series.idxmin()`: same as argmin in numpy
- `Series.nlargest(n)`: nth largest value
- `Series.values.flatten()`: becomes numpy arrays


## DataFrame Operations:
- delete column:  `df.drop('col', axis=1)`; `del df['col']`; `df.pop('col')`
- insert column:  `df.insert(pos, 'col', col_data)`; `df[:,'col']=value`
- rename column:  `df.rename(columns=map,inplace=True)`
- derive column:  `df.assign(new_col = some_transformation)`
- order by column:`df.sort_values(by, axis)`
- order by index: `df.sort_index(axis=0)` # useful for multi-index data
- select columns: `df[ [cols] ]`; `df['col']`
- exclude columns:`df.drop('col')`
- join columns:   `pd.concat([df's], axis=1, join)`; `pd.merge(left, right, how, on)`
- insert rows:    `df.append(new_rows, ignore_index=True)`, `pd.concat([df's],axis=0)`
- remove rows:    `df.dropna(axis=0, thresh)`
- query the data: `df.query('bool_expr')`
- transpose:      `df.T`
- summary:        `df.info()`; `df.describe()`
- check dtypes:   `df.dtypes`
- change dtype:   `df['col'].astype()`
- duplicates:    `df.duplicated(keep=False`): find all duplicated rows, keeping none of them
- plot:           Seaborn and Bokeh is great! `df.plot(kind='plot_kind_name')`; `df.plot.plot_type_name()`

|                               |                 |        |
|-------------------------------|-----------------|--------|
| Select column                 | `df[col]`       | Series |
| Select row by label           | `df.loc[label]` | Series |
| Select row by integer location| `df.iloc[loc]`  | Series |
| Slice rows                    | `df[5:10]`      | DataFrame |
| Select rows by boolean vector | `df[bool_vec]`  | DataFrame |

- `df.loc[:,:]` works only with labels
- `df.iloc[]` works only with integer index
- `df.loc[label]` uses row labels
- `df[label]` uses column labels
- `df.loc[:,:]= another_df` won't have any effect on `df` if `another_df` does not share the same shape

- Missing data:
  - `df.isnull()`
  - `df.notnull()`
  - `df.dropna()`

- Apply functions/mappings:
  - `df['col'].map(func)`   # point-wise mapping (similar to df['col'].apply, but df does not have map method)
  - `df.apply(func, axis)`  # along an axis
  - `df.applymap(func)`     # elementwise, df version of map

- Advanced transformation
  - `df.to_sql(name, con)`: write dataframe into a database; name is the table name; con is the SQLAlchemy engine


## Group by operations:
```
    bins = [0, 12, 50, 200]
    labels = ['0-12', '12-50', '50-200']
    df['new_col'] = pd.cut(df['old_col'], bins, labels=labels)

    groups = df.groupby('col')
    groups.aggregate(np.mean) # applies to all columns

    df.groupby('col').aggregate({'col1':np.mean, 'col2':np.size})

    groups.get_group(name='grp_name')

    groups['col']
```
- `df.groupby.apply(func)`      # apply to each sub data frame
- `df.groupby.agg(func)`        # apply to each column (Series) and return one value per column
- `df.groupby.filter(func)`     # filter on rows for each group and return a dataframe  
- `df.groupby.transform`  # 
- `df.groupby().unstack()`
- `df.groupby().cumcount()`
- `df.groupby().cumsum()`
- `df.groupby(level=0)`   # groupby also work on grouping by index


## Summary Tables
- `pd.melt(df, id_vars, value_vars)`: summarize data by value_vars (possible var value pair) against id_vars (rows)
- `pd.crosstab(index=col1, columns=[col2])`: create cross-tabulation between columns, by default is a frequency table
- `df.pivot_table(index='animal', columns='visits', values='age', aggfunc='mean')`: transpose categories into columns(http://pbpython.com/pandas-pivot-table-explained.html)


## Time Series Data
`df.shift()`
`df.rolling()`


## Pay Attention:
- chained indexing like `df[col][row]` is not reliable in returning view or copy
- remember to `reset_index` after transformations
- DataFrame and Series have its own plot functions
- unchained indexing like df.loc[:,:], df['col'] is the safest way to modify df
