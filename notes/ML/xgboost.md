# xgboost

## tree ensemble model
https://xgboost.readthedocs.io/en/latest/model.html  
tree ensemble model is a set of CART (Classification And Regression Trees), each CART produces a model score at each leave rather than decision values like the decision trees, final predictions is the sum of predictions from multiple trees  
random forest and boosted trees share the same model, but differs in the way they are trained

## parameter tuning
1. find `n_estimators`/`n_boost_round` with fixed high `learning_rate` using `xgb.cv`
2. find `max_depth` and `min_child_weight` using `GridSearchCV`, coarse tuning and fine tuning
3. find `gamma` using `GridSearchCV`
4. update `n_estimators`/`n_boost_round` using tuned parameters and `xgb.cv`
5. find `subsample` and `colsample_bytree` using `GridSearchCV`
6. find `reg_alpha` and `reg_gamma`
7. update to larger `n_estimators` with lower `learning_rate`

## tree boosting
additive training, meaning that add just one new tree at a time. Therefore the prediction at step t is based on step t-1, and the tree that optimizes the overall objective function( loss+reg ) is the one to be added  
explicitly define complexity of model (sum of complexity of trees) as the regularization: gamma*T + lambda*0.5*sum( (w_j)^2 ), (T: number of leaves, j: tree j)  
objective function at step t can be simplified and is specific to the new tree, which basically captures the quality of the tree structure (like the impurity measure in decision trees, but it also considers complexity)  

## model parameters
https://xgboost.readthedocs.io/en/latest//parameter.html  
model complexity: max_depth, gamma  
noise robustness: subsample, colsample_bytree  
imbalance: (scale_pos_weight, AUC) -> rank ordering; (max_delta_step) -> output real probability (can not re-balance)
