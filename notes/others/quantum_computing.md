# Quantum Computing

## Key Concepts and Principles  
#### Superposition (叠加态)

#### Entanglement (纠缠态)

#### Qubits
- Qubits can represent 1 and 0 **at the same time**, so one qubit stands for two numbers at the same time, three of them represent 8 numbers **at the same time**, while the classical bits can only represent 1 of the 8 numbers (they can represent different numbers, but at different time). 
- So doing an operation on such quantum computer is equivalent to **doing 8 operations at the same time and return 8 results** (true parallelism). Imagine that the there are hundreds of qubits, the computing power is exponential. 

#### Quantum States
- Quantum state can not be cloned (uncertainty principle -> can't be measured accurately)
- Observation changes the quantum state (collapse, think about measuring tire pressure with a gauge)
- Wave–particle duality, the particles can exist in multiple states at the same time
- Quantum particles can be in an entanglement state via certain interactions, such particles do not need to reside in the same physical location

#### Challenges & Limitations
- **Physical systems**:
    - quantum states are not stable. Minor perturbations in the environment (magnetic field, temperature, etc.) can change the states, before it can finish a calculation. Length of time a qubit living in a state is called **Coherence Time**
    - results may not be exactly accurate. Due to the delicate design and complicated operations, there is only certain chance of success, this is called **Fidelity**, e.g. 80%

    - number of qubits

- **Algorithms**
    - QC is not always advantageous in all computational problems. Simple arithemtics is one example. It is most effective in searching best solution among many candidates.


#### Quantum Computer
Software v.s. Hardware

- Classical computers rely on the 0/1 binary design principle and uses logical circuit as a physical way to implement such designs. Therefore a computer relies on both a high-level design principle and a physical system that can be accurately controlled to realize it (think of hi/lo voltages, ground/exited energe states, etc.)
- Quantum computers follows the same path. Qubits and different kinds of quantum gates are designed to achieve computations in a quantum setting. A physical quantum system is needed, which (1) has significant quantum-effects (2) can be easily controlled (3) is less error-prone. Finding one is very difficult and leads to multiple designs. 
- Currently, possible candidates include: Superconductors (more promising), Iron Traps, Quantum Optical systems, semiconduting quantum dots/2D gas. 
- A good QC should:
    - have a large number of extensible qubits
    - be able to intialize all qubits to ground states
    - have a long coherence time
    - be able to realize general quantum gate operations
    - be able to reliably measure the output 

#### Resources
- [Quantum computing is unconditionally safe](https://www.zhihu.com/question/20919153)
- [Bell measurement](https://web.archive.org/web/20181023105529/http://songshuhui.net/archives/84322)
- [General meachnism](https://web.archive.org/web/20181023103931/http://songshuhui.net/archives/84316)
- [Great analogies](http://blog.sciencenet.cn/blog-871282-996759.html)
- [当量子计算与上超导：一场美丽的邂逅](返朴)

## Quantum Algorithms
QC is most advantageous in searching /finding needle in a haystack type of problems.

- Grover's algorithm
- Shor's algorithm

error correction; how to output results after operations (measurement will lead to state collapse, so need repeated measurement to get all the results)

Currently, quantum algorithms are written in the form of qunatum operations (quantum gates) applied to (subsets of) qubits. Such representaion is called a quantum circuit. the quantum circuit then has to be transformed to be executed in a specific physical system, this is called quantum circuit compilation. Typically, the same algorithm can be represented with different quantum circuits, but only the ones with shorter execution time are preferred, due to decoherence.

## Quantum Telecommunication
The movement of qubits does require the movement of "things"; in particular, the actual teleportation protocol requires that an entangled quantum state or Bell state be created, and its two parts shared between two locations (the source and destination, or Alice and Bob). 

In essence, a certain kind of "quantum channel" between two sites must be established first (entanglement), and a classical channel is also needed.

An entangled system is defined to be one whose quantum state cannot be factored as a product of states of its local constituents; that is to say, they are not individual particles but are an inseparable whole.

Entanglement is broken when the entangled particles decohere through interaction with the environment; for example, when a measurement is made.

Quantum systems can become entangled through various types of interactions. 

Particles always exist in different places with different probabilities in a microscopic world, and each measurement of the state of such particles will ineivtably change its state to some extent

Quantum state can not be cloned (**uncertainty principle**), which gives the **best safety** but also creates the hardest problem for communication (copying info and sending it over). With the help of a classical channel and a quantum channel (entanglement), it is possible to safely send a quantum state. (Alice wants to send X to Bob, Alice uses a phone and a quantum pair A&B, Alice has A while Bob has B; Alice makes a **bell measurement** on X and A, X collapses, Alice calls Bob to tell him the transformations she made, Bob makes corresponding transformations on B; Now X is gone, B becomes X, Bob has an exact copy of X)

## Encryption and being Quantum-safe:
- Encription is essentially finding difficult math problems that are hard to solve for classical computers.
- There are two types of encription mechanisms: deterministic keys vs. non-deterministic keys
- QC will threat existing algorithms by either invalidifying them or asking for increasing input/output sizes.
- what is quantum-safe
