# SAS
## Files in SAS
- rawdata file
- SAS Datasets
- SAS Program files

## Steps and Syntax
- SAS programs are consisted of **DATA** steps and/or **PROC** steps
    + Begin of SAS steps: `DATA`/ `PROC`
    + End of SAS steps: `RUN` or `QUIT` or beginning of another `DATA`/ `PROC`
    + Statements end with semicolon(`;`)
- `DATA` step:
    + Read in rawdata to process and create SAS dataset
- `PROC` step:
    + process and print results
- Statement
    + starts with a key word (identifier), followed by options, ends with `;`
    + Global Statements:
        + lie outside of steps, can affect multiple steps.
- Comment:
    + block comments: `/* */`; do not start from the first/second column (indented start)
    + line comments:  `*comments;`; must end with `;`; can start in any column
- Reference SAS date: *1960/01/01*
- SAS date constant: *'ddmmm<yy>yy'D*; can be used in comparison; e.g. ` '11Jan2011'D `
- Log: errors/ warnings/ notes
- Results: output

## SAS Data Set
### General Info
- it is a SAS-specific format. In analogy to SQL, Table<->Data Set; Row<->Observation; Column<->Variable;
- *descriptor* portion and *data* portion
    + *descriptor*: general properties and variable properties; use `PROC CONTENTS` to print
    + *data*: variable names and values; use `PROC PRINT` to print
- Missing values: '.' for missing numeric; '' for missing character
- Data types: *char* (1 byte/character) and *num* (8 bytes by default)
- SAS Library
    + a SAS Library stores a collection of SAS Datasets (members) as a single unit
    + at the start of each SAS session, one temporary library and at least one permanent library is provided
    + example permanent library: sashelp, sasuser
    + work is temporary library, whose contents will be deleted at the end of a SAS session
    + physical library locations are referred to by `libref`, the library reference name, e.g. sashelp, work (just like package names in python import)
    + SAS datasets can be accessed in the format of `libref.dataset`, default `libref` is `work`, all datasets in permanent libraries have to give explicit `libref` names for access.
    + Define a library: `LIBNAME libref 'filepath'`; `libref` must be less than 8 characters
    + Clear a library: `LIBNAME libref clear`
    + Examine a library: `PROC CONTENTS DATA=libref._ALL_ <nods>; run;`
    + Examine a dataset: `PROC PRINT DATA=libref.dataset`

### Create SAS Dataset
- Build dataset from an input dataset with subsetting
```
DATA output-dataset-name;
  SET input-dataset-name;
  WHERE expressions;
RUN;
```
- Row subsetting: `WHERE`, `CONTAIN`, `LIKE`
- Row subsetting on newly created variables: `IF expression`:
    + excludes data from the output, unlike `WHERE` from the input
    + can't use the clauses used with `WHERE`
    + can only be used in `DATA` steps, not in `PROC`
- Add/Modify columns (assignment): `var=expression`
- Column subsetting: `DROP`, `KEEP`
- Relabel variables: `LABEL var1='new_name'`; this is permanent, the label is added to the descriptor portion


## Reporting by `PROC PRINT`
### `PROC PRINT`
- select variables: `VAR var1 var2;`
- summarize variables: `SUM var1 var2;` (displayed on the last row)
- filter observations: `WHERE expression`;
    + expression can only contain variables in the SAS dataset
    + (in, ~=,^=) for comparing; (&, |, ~, ^) for combining expressions; one can also use (AND, OR, NOT)
    + substring matching: `CONTAIN "string"`; `LIKE "pattern"`; pattern in SAS uses `%` and `_`
    + augment previous `WHERE` clause: `WHERE SAME and more-expressions`
    + missingness: `IS MISSING`; `IS NULL`
    + compare ranges: `BETWEEN ... AND ...`
- index observations by other variable rather than `obs`: `ID variable`
- group by variable: `BY var1 var2;`. But the input dataset has to be _already sorted_ on the var1 and var2 in `BY`
- relabel variables: `LABEL var1='label1' var2='label2';`
    + need to add `PROC PRINT LABEL` upfront;
    + it is temporary, meaning that the labels are not saved into the descriptor portion of the dataset

### `PROC SORT`
```
PROC SORT DATA=dataset <OUT=new_dataset>;
  BY <DESCENDING> var1 var2;
RUN;
```

### Title and Footnote
- title on the `n`th line: `TITLEn 'title_name';`
- footnote on the `n`th line: `FOOTNOTEn 'footnote_name';`
- title/footnote are global statement and needs to be canceled after use, by `TITLE;` or `FOOTNOTE;`

### Format Value Printing
- `FORMAT var1 format`
- SAS format follows the pattern of `<$>format_name<w>.<d>`; `$` indicates a string format; `w` and `d` are total width and decimal digits;
- User defined formats:
    + range: `'B'-'E'`; `1-6`; `1<-<10`; `low<-100`; `100<-<high`;
    + `$` indicates a character variable;
```
PROC FORMAT;
  VALUE <$>format_name value-or-range1='formatted value1';
RUN;
```
