# BeautifulSoup 4
It's a parsing library for HTML and XML files. It's usually used along with packages like `re, urllib2, lxml`. By default, content are encoded by `Unicode`.

## Initialize BS object
```
from bs4 import BeautifulSoup
file_handle = open('path/to/file')
soup = BeautifulSoup(file_handle, 'xml', from_encoding='UTF-8')
print(soup.prettify())
file_handle.close()
```

## Navigate through document
*soup, the parser tree*
- a deeply-nested and well-connected data structure that corresponds to the file content/structure.
- contains `Tag` and `NavigableString` sub-objects, referring to `"<html>blah<\html>"` and `"this is a paragraph"` type of contents.
- common attributes, returned as ordered list or string, or iterator:
  + tags as attributes, `soup.html/ head/ body/ title/ p`
  + up/down one level:  `parent`/ `contents` / `children`
  + on the same level:  `next_sibling`/ `previous_sibling`
  + by order rather then level: `next_element`/ `previous_element`
  + convert to Python string:   `unicode()`

## Search in document
Argument lists are almost identical across all filter functions
- arguments: string / regex like `re.compile("pat")` / list of strings / `True` / functions (boolean)
- meanings:  exact match / pattern match / match any item / match everything / DIY
- all matches: `find_all()`, filter on Tag or a specific attribute
- first match: `find()`
- find different portions of document: `find_parent()`, `find_sibling`, `find_next`, etc.

## Print document
- `prettify()`
- `renderContents()`
- `str()`

## Modify the contents

## Notes
- Does not work very well with XML, b/c XML does not have a fixed set of tags and there may be self-closing tags in the document
- Beautiful Soup stores only Unicode strings in its data structures.
