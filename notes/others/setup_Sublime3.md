# Setup Sublime Text 3 as an IDE

## Install Package Control
- Markdown Preview
- SideBarEnhancements
- Anaconda
- requirementstxt
- SublimeLinter (linters are separately installed, using `SublimeLinter-[linter_name]`)
- GitGutter
- SublimeGit (spend some time on the documentation)
- FTPSync

## Settings
### Preference
[Unofficial Documentation](http://sublime-text-unofficial-documentation.readthedocs.org/en/latest/reference/settings.html)

```json
{
	  // base settings
	"font_size": 15,
	"rulers": [80, 120],
	"tab_size": 4,
	"word_wrap": true,
	//"auto_complete": false,
	"sublimelinter": false,
	"bold_folder_labels": true,
	"detect_indentation": true,
	"draw_indent_guides": true,
	"ensure_newline_at_eof_on_save": true,
	"file_exclude_patterns":
	[
        "*.DS_Store",
        "*.pyc",
        "*.git"
	],
	"find_selected_text": true,  
	"folder_exclude_patterns":
	[
	],
	"highlight_line": true,
    "highlight_modified_tabs": true,
    "remember_open_files": true,
    "remember_open_folders": true,
}
```
### Python.sublime-settings

```json
	"tab_size": 4,
	"word_wrap": true,
	"wrap_width": 80,
	"translate_tabs_to_spaces": true,
	"trim_trailing_white_space_on_save": true,
	"ensure_newline_at_eof_on_save": true
```

### Theme
Preview before installing: [colorsublime](https://colorsublime.github.io)


## Keyboard Shortcuts

## Split Layout
Especially useful in
- test-driven development (code + tests)
- front-end development (HTML + CSS)


