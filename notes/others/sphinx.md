## Setup

### Install `sphinx`
1. create new python env
2. `pip install sphinx`

### Build basic layout/files
3. `sphinx-quickstart docs`
4. `sphinx-build -b html docs/source/ docs/build/html`


## Customize
### General process
5. add/update `.rst` files in `docs/source/`
6. rebuild the doc:
```
cd docs
make html
```

### Specific tasks
#### Change appearnce
7. install extensions/themes via `pip` 
8. update `config.py`
9. `make html`

#### Expand documentation
10. create a new `.rst` file under `source/`
11. add a toctree directive at the end of `index.rst`

#### Add cross reference
12. use `:doc:` to reference `.rst` files
13. use `:ref:` to refer to any part of a document

#### Add code documentation
##### Manually add code tests:
14. Add code modules' paths to sphinx so it can run the code, by:
```
import pathlib
import sys
sys.path.insert(0, pathlib.Path(__file__).parents[2].resolve().as_posix())
```
15. enable `doctest` extension in `config.py` by:
```
extensions = ['sphinx.ext.doctest']
```
16. write code to be executed in `.rst` with prefix `>>>`

##### Semi-automated code doc generation:
17. enable extension of `sphinx.ext.autodoc` in `config.py`

18. add in `.rst` file the following:

    `.. autofunction:: import.path.to.function` (for including docstrings)

19. add other python objects in the '.rst' file, e.g. `.. autoexception:: `

##### Automated API reference generation
20. enable extension of `sphinx.ext.autosummary`
21. add new `api.rst` file and write:
```
API
===

.. autosummary::
   :toctree: generated
```
22. add `api.rst` to `index.rst`'s `toctree`

