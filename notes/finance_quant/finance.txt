cash and equilvalents (anything found in a traditional bank)

fixed income (bonds)
interst rate goes up -> bonds value goes down

equities (stocks)

hard assets

mutual funds
active vs. passive management; passive tend to perform better
  in 401k:
  target-date portfolios
  may become too conservative too quickly, but can change the target date
  analyze the fund's fact sheet (historic return and risk, longer span of time)
  statefarm 401k has very low fees compared to others

exchange-traded funds (ETF)
good for investing with large amount each time

monitor the asset allocation, rebalance the allocation say every year


consider when money is needed
if money is not needed in 10 years, can invest more aggressively to achieve capital growth

bonds is good for maintaining purchasing power

look at longer period of time for return and loss for a mutual fund and also applies to your other investing strategies
look at stock index or bond index and compare them to an individual stock/bond to determine it is doing good than average or not
Money, Kiplinger's Personal Finance is a good subscription

rebalance your portfolio roughly yearly, check if the allocation is changing after one year, and sell and buy to make the ratios roughly the same
Asset allocation tool in my benefit resources.
target fund is designed for all-in or all-out

estate planning, get:
Durable power of attorney
health care power of attorney
(can get from a bank/lifeworks and hospital)
living will

state farm group term life insurance is provided to all employees
529 plan to save for education of children (future 4-year college would be 70k to 120k)
ROTH IRA is good for one child, but 529 is better for more children

a good inflation rate used for calculation: 3.5%

retirement resource: pension ($1 per day), 401k, personal resource, social security
my statefarm benefits resources -> retirement plan projection -> calculation

roth 401k: tax bracket equal or lower today than retirement
15% is typical tax rate for most people retired

IRA vs. 401K, each one has a traditional and Roth versions, your tax brackets determine which is better
