Dow Jones Index
- an average index that tracks the top (market-cap) 30 public companies in the USA, listed either in NYSE or NASDAQ
- high priced stocks are weighted more
- a proxy for US economy
- lower volatility

NASDAQ
- an electronic exchange platform consists of ~3000 companies/stocks.
- also used as an index of the stocks traded there (weighted by market cap)
- mainly tech sectors and companies in growth stages
- higher volatility
- NASDAQ to Dow Ratio: higher value (>0.3) indicates bullish market; lower value (<0.18) indicates bearish market 

S&P 500
- an average index that tracks the 500 largest companies from NYSE and NASDAQ.
- no stock price weighting, but weights are based on actual company size
- mirror the diversity (percentages) of the largest companies on the NYSE and Nasdaq
- a borader and smoother measure of US economy
- tracks closely with Dow Jones
- lower volatility than Dow Jones

NYSE
- similar to NASDAQ, a trading platform, but also can be used as an index
