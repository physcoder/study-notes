# Stochastic Process:
- *definition*: A collection of random variables, indexed by time
    + continuous stochastic process, ${X_t} (t>=0)$
    + discrete stochastic process, $x_0, x_1, x_2, ...$
- realization of a stochastic process, a screenshot (observation) of the RV's values by a certain timestamp t; it gives one path of the stochastic process before time t
- *alternative definition*: based on the concept of realization; the probability distribution over a space of paths
    + e.g. `f(t) = t` with p=1/2; `f(t) = -t` with p=1/2. Only two paths possible, value at one time known, values at all other times are known
    + e.g. `f(t) = {t, p=1/2; -t, p=1/2}`, infinite number of paths, at each time point, X can take two possibilities
- type of questions to answer:
    + what are the dependencies in the sequence of values? (past and future connection)
    + what are the long term behaviors of the sequence? (law of large number; central limit theorem)
    + what are the boundary events? (extreme situations, price dropping 10% for 5 consecutive days)

## Simple Random Walk
*Definitions*:
For a collection of i.i.d. random variables $Y_i = {1, P=p; -1, P=1-p}$, define $Xt = \sum_i Y_i$, Xt is a simple random walk; Xt is a symmetric simple random walk when p=1/2
*Properties (p=1/2)*:
- $E(X_k) = E(X_0) = 0$
- for non-overlapping time points {t0, t1, ..., tn}, X_{t_{n+1}} - X_{t_{n}} are mutually independent


## Markov Chain

## Martingale
*Definitions*
$E(X_{t+1}|F_t) = X_t, F_t = {X_0, X_1, ..., X_t}$
